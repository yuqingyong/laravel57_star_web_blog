/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : yqyblog

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-05-17 17:02:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `yqy_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `yqy_auth_rule`;
CREATE TABLE `yqy_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yqy_auth_rule
-- ----------------------------
INSERT INTO `yqy_auth_rule` VALUES ('1', '0', '/admin/index', '后台首页', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('2', '1', '/admin/login_out', '退出登录', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('3', '1', '/admin/clear_cache', '清空缓存', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('4', '1', '/admin/website', '基本信息', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('5', '1', '/admin/edit_pass', '修改密码', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('6', '0', '/admin/member_list', '会员列表', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('7', '6', '/admin/member_status', '禁用会员', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('8', '0', '/admin/chat_list', '闲言碎语', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('9', '8', '/admin/chat_status', '闲言状态', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('10', '8', '/admin/chat_add', '添加闲言', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('11', '8', '/admin/chat_edit', '修改闲言', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('12', '8', '/admin/chat_del', '删除闲言', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('13', '0', '/admin/comment_list', '用户评论', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('14', '13', '/admin/comment_status', '禁用评论', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('15', '13', '/admin/comment_del', '删除评论', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('16', '0', '/admin/demand_list', '需求列表', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('17', '16', '/admin/demand_status', '禁用需求', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('18', '16', '/admin/demand_del', '删除需求', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('19', '0', '/admin/message_list', '留言列表', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('20', '19', '/admin/message_status', '禁用留言', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('21', '19', '/admin/message_del', '删除留言', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('22', '0', '/admin/category_list', '文章分类', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('23', '22', '/admin/category_add', '添加分类', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('24', '22', '/admin/category_edit', '修改分类', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('25', '22', '/admin/category_del', '删除分类', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('26', '0', '/admin/tag_list', '文章标签', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('27', '26', '/admin/tag_add', '添加标签', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('28', '26', '/admin/tag_edit', '修改标签', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('29', '26', '/admin/tag_del', '删除标签', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('30', '0', '/admin/advert_list', '广告列表', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('31', '30', '/admin/advert_status', '禁用广告', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('32', '30', '/admin/advert_add', '添加广告', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('33', '30', '/admin/advert_edit', '修改广告', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('34', '30', '/admin/advert_del', '删除广告', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('35', '0', '/admin/friendurl_list', '友情链接', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('36', '35', '/admin/friendurl_add', '添加链接', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('37', '35', '/admin/friednurl_edit', '修改链接', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('38', '35', '/admin/friendurl_del', '删除链接', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('39', '0', '/admin/article_list', '文章列表', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('40', '39', '/admin/article_add', '添加文章', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('41', '39', '/admin/add_article_tag', '添加文章标签', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('42', '39', '/admin/article_status', '文章状态', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('43', '39', '/admin/article_edit', '修改文章', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('44', '39', '/admin/article_del', '删除文章', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('45', '39', '/admin/article_recovery', '回收站', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('46', '39', '/admin/recovery', '恢复文章', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('47', '39', '/admin/del_all', '彻底删除', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('52', '0', '/admin/rule_list', '权限列表', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('53', '52', '/admin/rule_add', '添加权限', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('54', '52', '/admin/rule_edit', '修改权限', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('55', '52', '/admin/rule_del', '删除权限', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('56', '0', '/admin/auth_group', '管理分组', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('57', '56', '/admin/group_add', '添加分组', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('58', '56', '/admin/group_edit', '修改分组', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('59', '56', '/admin/group_del', '删除分组', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('60', '0', '/admin/admin_user', '管理员列表', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('61', '60', '/admin/admin_user_add', '添加管理员', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('62', '60', '/admin/admin_user_edit', '修改管理员', '1', '1', '');
INSERT INTO `yqy_auth_rule` VALUES ('63', '60', '/admin/admin_user_del', '删除管理员', '1', '1', '');
