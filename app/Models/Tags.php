<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model{
    # 设置表格
    protected $table = 'tags';

    # 设置主键
    protected $primaryKey = 'tid';

    # 设置是否开启时间戳维护
    public $timestamps = false;

    protected $fillable = ['tname'];


}