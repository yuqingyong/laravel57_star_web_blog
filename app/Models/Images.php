<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $fillable = ['type','path'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','uid');
    }
}
