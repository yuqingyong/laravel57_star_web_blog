<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Message extends Model{
    # 设置表格
    protected $table = 'message';

    # 设置主键
    protected $primaryKey = 'id';

    # 设置是否开启时间戳维护
    public $timestamps = false;

    # 设置可操作字段
    protected $fillable = ['contents','nickname','create_time'];

}