<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model{
    # 设置表格
    protected $table = 'chat';

    # 设置主键
    protected $primaryKey = 'chid';

    # 设置是否开启时间戳维护
    public $timestamps = false;


}