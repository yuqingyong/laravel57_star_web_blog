<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ArticlePic extends Model{
    # 设置表格
    protected $table = 'article_pic';

    # 设置主键
    protected $primaryKey = 'ap_id';

    # 设置是否开启时间戳维护
    public $timestamps = false;

    protected $fillable = ['path', 'aid'];


}