<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Demand extends Model{
    # 设置表格
    protected $table = 'demand';

    # 设置主键
    protected $primaryKey = 'xid';

    # 设置是否开启时间戳维护
    public $timestamps = false;

    # 可操作字段
    protected $fillable = ['title','contents','demand_type','uid','create_time'];

    # 关联用户
    public function headimg()
    {
        return $this->belongsTo('App\Models\User','uid')->select('head_img','nickname','username','created_at','updated_at');
    }


}