<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Category extends Model{
    # 设置表格
    protected $table = 'category';

    # 设置主键
    protected $primaryKey = 'cid';

    # 设置是否开启时间戳维护
    public $timestamps = false;

    protected $fillable = ['cname', 'description','cid'];

    public function article()
    {
        return $this->belongsTo('App\Models\Article','cid');
    }


}