<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Article extends Model
{
    # 使用软删除
//    use SoftDeletes;

	# 设置表格
	protected $table = 'article';
	# 设置主键
	protected $primaryKey = 'aid';
	
	# 设置是否开启时间戳维护
	public $timestamps = true;

	# 设置时间格式
    public function getDateFormat()
    {
        return time();
    }

//    # 需要被转换成日期的属性
//    protected $dates = ['deleted_at'];

    # 设置反向关联,关联文章分类
    public function category()
    {
        return $this->belongsTo('App\Models\Category','cid');
    }

    # 设置一对一关联,关联文章封面
    public function articlepic()
    {
        return $this->hasOne('App\Models\ArticlePic','aid');
    }

    # 可以添加更新的字段
	protected $fillable = ['title','cid','content','author','deleted_at','keywords','is_show','sort','click','comment_num','description'];
    
	/**
	 * 查询文章
	 * @param   $cid   分类条件      
	 * @param   $tid   标签ID      
	 * @param   $is_show   是否显示      
	 * @return  $data  返回数据
	 */
	public function  getPageData($cid = 'all',$tid='all',$is_show='1',$field = '*',$delete_at=0,$limit=10)
	{
		if($cid == 'all' && $tid == 'all'){
			// 获取全部分类、全部标签下的文章
			if($is_show == 'all'){
				$where = ['deleted_at'=>$delete_at];
			}else{
				$where = ['deleted_at'=>$delete_at,'is_show'=>$is_show];
			}
			$list = DB::table('article')
				  ->join('article_pic','article.aid','=','article_pic.aid')
				  ->where($where)->orderBy('article.sort','desc')->select($field)->paginate($limit);
		} elseif ($cid == 'all' && $tid != 'all') {
			//查询该标签下的所有文章
			if($is_show == 'all'){
				$where = ['article.deleted_at'=>$$delete_at,'article_tag.tid'=>$tid];
			}else{
				$where = ['article.deleted_at'=>$delete_at,'is_show'=>$is_show,'article_tag.tid'=>$tid];
			}
			$list = DB::table('article_tag')
				  ->join('article','article_tag.aid','=','article.aid')
				  ->join('article_pic','article.aid = article_pic.aid')
				  ->where($where)->orderBy('article.sort','desc')->select($field)->paginate($limit);
		} elseif ($cid!='all' && $tid=='all') {
			//查询该分类下的所有文章
			if($is_show == 'all'){
				$where = ['deleted_at'=>$delete_at,'cid'=>$cid];
			}else{
				$where = ['deleted_at'=>$delete_at,'is_show'=>$is_show,'cid'=>$cid];
			}
			$list = Db::table('article')
				  ->join('article_pic','article.aid = article_pic.aid')
				  ->where($where)->orderBy('article.sort','desc')->select($field)->paginate($limit);

		}
		
		return $list;
	}
	
	/**
     * 添加文章封面并推送     
     * @return  $resu  图片添加状态
     */
	public function add_article_pic($content)
	{
		$aid = DB::table('article')->orderBy('aid','desc')->select('aid')->first();
        $info= '.'.getpic($content);
		if($info === '.') {
			$info = './upload/article.jpg';
		}
		$thumbnail_file_path = "./upload/".$aid->aid."thumb.png";
        //生成缩略图
		Image::make($info)->resize(200, 200, function ($constraint) {$constraint->aspectRatio();})->save($thumbnail_file_path);
		
		$p['path']= "/upload/".$aid->aid."thumb.png";
		$p['aid'] = $aid->aid;

		//将得到的图片路径添加至图片表
		$resu = DB::table('article_pic')->insert($p);
		//百度推送文章
		$urls = array();
		$urls[] = "http://www.yuqingyong.cn/news_detail/".$aid->aid;
		bdUrls($urls);
		if($resu){return true;}else{return false;}
	}

	/**
     * 获取文章评论列表
     * @param aid 文章ID
     * @return $comment
     */
	public function getComments($aid)
    {
        $comments = DB::table('comment')->join('users','comment.uid','=','users.uid')->where(['aid'=>$aid,'comment.status'=>1])->select('comment.content','comment.created_at','comment.email','users.nickname','users.head_img')->get();
        return $comments;
    }
	
}
