<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ArticleTag extends Model{
    # 设置表格
    protected $table = 'article_tag';

    # 设置主键
    protected $primaryKey = 'id';

    # 设置是否开启时间戳维护
    public $timestamps = false;

    protected $fillable = ['aid','tid'];

    # 关联文章列表
    public function articles()
    {
        return $this->belongsTo('App\Models\Article','aid')->select('aid','title','created_at','updated_at','cid','author','keywords','click','comment_num','description');
    }

    # 关联标签列表
    public function tagname()
    {
        return $this->belongsTo('App\Models\Tags','tid');
    }


}