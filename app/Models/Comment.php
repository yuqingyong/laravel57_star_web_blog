<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model{
    # 设置表格
    protected $table = 'comment';

    # 设置主键
    protected $primaryKey = 'cmtid';

    # 设置是否开启时间戳维护
    public $timestamps = false;

    protected $fillable = ['uid','aid','contents','date','email','created_at'];

    # 关联用户
    public function headimg()
    {
        return $this->belongsTo('App\Models\User','uid')->select('head_img','nickname','username','created_at','updated_at');
    }
}