<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Advert extends Model{
    # 设置表格
    protected $table = 'advert';

    # 设置主键
    protected $primaryKey = 'mid';

    # 设置是否开启时间戳维护
    public $timestamps = false;

    protected $fillable = [];


}