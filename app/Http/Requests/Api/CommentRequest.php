<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'aid' => 'required|regex:/^[1-9]\d*$/',
            'contents' => 'required|string',
            'email' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'aid.regex' => '文章参数错误',
            'contents.required' => '评论内容必须',
            'contents.string' => '文章内容必须为字符串',
            'email.email' => '邮箱格式错误'
        ];
    }
}
