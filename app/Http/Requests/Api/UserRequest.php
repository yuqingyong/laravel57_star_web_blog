<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|between:3,25|regex:/^[A-Za-z0-9\-\_]+$/|unique:users,username',
            'password' => 'required|string|min:6',
            'verification_key' => 'required|string',
            'verification_code' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'username.required' => '名称必须',
            'username.between' => '名称必须3到25个字符',
            'username.regex' => '名称必须是字母和数字组合',
            'username.unique' => '名称已被注册',
            'password.required' => '密码必须',
            'password.string' => '密码必须是字符串',
            'password.min' => '密码最少6位',
            'verification_key.required' => '验证码key必须',
            'verification_key.string' => '验证码key必须是字符串',
            'verification_code.required' => '验证码必须',
            'verification_code.string' => '验证码必须是字符串',
        ];
    }

}
