<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class DemandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:35',
            'contents' => 'required|string',
            'demand_type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => '标题必须',
            'title.max' => '标题最多35个字符',
            'contents.string' => '内容必须为字符串',
            'demand_type' => '请选择正确的类型'
        ];
    }
}
