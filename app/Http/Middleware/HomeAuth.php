<?php

namespace App\Http\Middleware;

use Closure;
use session;

class HomeAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if(!session('users')){
			if($request->ajax()){
                echo json_encode(['status'=>2000,'msg'=>'请先登录']);exit;
            } else {
                return redirect('/prompt')->with(['message'=>'请先登录','url' =>'/', 'jumpTime'=>3,'status'=>'error']);
            }
		}

        return $next($request);
    }
}
