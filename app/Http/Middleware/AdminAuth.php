<?php

namespace App\Http\Middleware;

use App\Models\Auth;
use Closure;
use session;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if(!session('admin_user')){
			return redirect('/yqylogin');
		} else {
            # 验证权限
            $uid = session('admin_user.id');
            $path = $request->route()->getAction();
            $rule = $path['prefix'].'/'.substr($path['controller'],strpos($path['controller'],'@')+1);
            # 权限验证
            $auth = new Auth();
            $result = $auth->check($rule,$uid);
            if(!$result){
                if($request->isMethod('post')){
                    json_return(1000,'暂无权限！');
                } else {
                    return redirect('/prompt')->with(['message'=>'您没有权限访问！','url' =>'/admin/index', 'jumpTime'=>3,'status'=>'error']);
                }
            }
            
        }
        return $next($request);
    }
}
