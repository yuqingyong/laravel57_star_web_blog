<?php

namespace App\Http\Controllers\Api;

use App\Transformers\ChatTransformer;
use Illuminate\Http\Request;
use App\Models\Chat;

class ChatController extends Controller
{
    // 获取聊天列表
    public function chat_list(Chat $chat)
    {
        $query = $chat->query();
        $chat_list = $query->where('is_show','1')->orderBy('chid','desc')->get();
        return $this->response->collection($chat_list,new ChatTransformer());
    }


}
