<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\ImageRequest;
use App\Handlers\ImageUploadHandler;
use App\Models\Images;
use App\Transformers\ImageTransformer;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    public function store(ImageRequest $request,ImageUploadHandler $uploader,Images $image)
    {
        $user = $this->user();

        $size = $request->type == 'avatar' ? 360 : 1024;
        $result = $uploader->save($request->image, str_plural($request->type), $user->id, $size);

        $image->path = $result['path'];
        $image->type = $request->type;
        $image->uid = $user->uid;
        $image->save();

        return $this->response->item($image, new ImageTransformer())->setStatusCode(201);
    }
}
