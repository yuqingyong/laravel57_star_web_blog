<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\MessageRequest;
use App\Models\Message;
use App\Transformers\MessageTransformer;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    // 留言列表
    public function message_list(Message $message)
    {
        $query = $message->query();
        $message_list = $query->where('is_show','1')->orderBy('id','desc')->get();
        return $this->response->collection($message_list,new MessageTransformer());
    }

    // 添加留言
    public function addmessage(MessageRequest $request,Message $message)
    {
        $data['contents'] = htmlspecialchars($request->contents);
        $data['nickname'] = htmlspecialchars($request->nickname ?: '游客');
        $data['create_time'] = time();

        $res = $message->create($data);

        return $this->response->item($res,new MessageTransformer())->setStatusCode(201);
    }
}
