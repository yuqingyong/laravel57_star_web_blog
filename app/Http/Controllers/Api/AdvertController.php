<?php

namespace App\Http\Controllers\Api;

use App\Models\Advert;
use App\Transformers\AdvertTransformer;
use Illuminate\Http\Request;

class AdvertController extends Controller
{
    // 获取banner列表
    public function advert_list(Advert $advert)
    {
        $query = $advert->query();
        $advert_list = $query->orderBy('mid','desc')->get();
        return $this->response->collection($advert_list,new AdvertTransformer());
    }


}
