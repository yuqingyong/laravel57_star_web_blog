<?php

namespace App\Http\Controllers\Api;

use App\Transformers\TagsTransformer;
use Illuminate\Http\Request;
use App\Models\ArticleTag;
use App\Models\Tags;
use App\Transformers\ArticleTagTransformer;

class TagController extends Controller
{
    // 获取标签列表
    public function tag_list(Tags $tags)
    {
        $query = $tags->query();
        $tags_list = $query->orderBy('tid','desc')->get();
        return $this->response->collection($tags_list,new TagsTransformer());
    }


    // 根据标签查询文章列表
    public function article_tag_list(Request $request,ArticleTag $articletag)
    {
        $query = $articletag->query();
        if ($tid = $request->tid) {
            $query->where('tid', $tid);
        } else {
            return $this->response->array(['err_code'=>101,'err_msg'=>'请选择标签']);
        }
        $articles = $query->paginate(10);
        return $this->response->paginator($articles, new ArticleTagTransformer());
    }
}
