<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CommentRequest;
use App\Models\Comment;
use App\Transformers\CommentTransformer;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    // 添加评论
    public function addcomment(CommentRequest $request,Comment $comment)
    {
        $comment->uid = $this->user()->uid;;
        $comment->aid = $request->aid;
        $comment->contents = htmlspecialchars($request->contents);
        $comment->created_at = time();
        $comment->email = $request->email;
        $comment->save();

        return $this->response->item($comment,new CommentTransformer())->setStatusCode(201);
    }
}
