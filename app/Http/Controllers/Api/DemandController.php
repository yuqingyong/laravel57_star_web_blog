<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\DemandRequest;
use App\Models\Demand;
use App\Transformers\DemandTransformer;

class DemandController extends Controller
{
    // 获取需求列表
    public function demand_list(Demand $demand)
    {
        $query = $demand->query();
        $demand_list = $query->orderBy('xid','desc')->paginate(10);
        return $this->response->paginator($demand_list,new DemandTransformer());
    }

    # 添加需求
    public function add_demand(DemandRequest $request,Demand $demand)
    {
        $data = $request->all();
        $data['title'] = htmlspecialchars($request->title);
        $data['contents'] = htmlspecialchars($request->contents);
        $data['create_time'] = time();
        $data['uid'] = $this->user()->uid;
        $res = $demand->create($data);

        return $this->response->item($res,new DemandTransformer())->setStatusCode(201);
    }

}
