<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DemandController extends Controller
{
    // 需求列表
	public function demand_list()
	{
		$demands = DB::table('demand')
				  ->join('users','users.uid','=','demand.uid')
				  ->select('demand.title','demand.demand_type','demand.contents','users.username','demand.is_show','xid','demand.see_num','demand.comment_num','demand.create_time')
				  ->orderBy('xid','desc')->paginate(20);
		return view('admin/demand_list',['demands'=>$demands]); 
	}
	
	// 禁用列表
	public function demand_status(Request $request)
	{
		if($request->isMethod('post')){
			$xid = $request->input('xid');
			$status = $request->input('status');
			if(!preg_match('/^[1-9]\d*$/',$xid)) json_return(1000,'参数错误');
			if(!preg_match('/^[0-1]\d*$/',$status)) json_return(1000,'参数错误');
			# 修改状态
			$res = DB::table('demand')->where('xid',$xid)->update(['is_show'=>$status]);
			if($res){
				json_return(200,'操作成功');
			} else {
				json_return(200,'操作失败');
			}
		} else {
			json_return(1000,'非法操作');
		}
	}
	
	// 删除列表
	public function demand_del(Request $request)
	{
		if($request->isMethod('post')){
			$xid = $request->input('xid');
			if(!preg_match('/^[1-9]\d*$/',$xid)) return redirect('/prompt')->with(['message'=>'参数错误','url' =>'/admin/demand_list', 'jumpTime'=>3,'status'=>'error']);
			
			# 删除数据
			$res = DB::table('demand')->where('xid',$xid)->delete();
			if($res){
				json_return(200,'删除成功');
			} else {
				json_return(1000,'删除失败');
			}
		}
	}
	
	
	
}
