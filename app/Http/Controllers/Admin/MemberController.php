<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class MemberController extends Controller
{
    // 会员列表
	public function member_list(Request $request)
	{
		if($request->isMethod('post')){
			$kw = $request->input('word','','htmlspecialchars');
			$members = DB::table('users')->where('username','like',$kw.'%')->orderBy('uid','desc')->paginate(20);
		} else {
			$members = DB::table('users')->paginate(20);
		}
		return view('admin/member_list',['members'=>$members]); 
	}
	
	// 禁用会员
	public function member_status(Request $request)
	{
		if($request->isMethod('post')){
			$uid = $request->input('uid');
			$status = $request->input('status');
			if(!preg_match('/^[1-9]\d*$/',$uid)) json_return(1000,'参数错误');
			if(!preg_match('/^[0-1]\d*$/',$status)) json_return(1000,'参数错误');
			# 修改状态
			$res = DB::table('users')->where('uid',$uid)->update(['status'=>$status]);
			if($res){
				json_return(200,'操作成功');
			} else {
				json_return(200,'操作失败');
			}
		} else {
			json_return(1000,'非法操作');
		}
	}
	
	
	
	
}
