<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use session;
use DB;

class IndexController extends Controller
{
    // 首页
	public function show()
	{
		$group_id = session('admin_user.group');
		# 获取权限规则
		$group = DB::table('auth_group')->where('id',$group_id)->select('rules')->first();
		# 获取权限列表
		$rule_arr = explode(',', $group->rules);
		$rule = [];
		foreach ($rule_arr as $k => $v) {
			$auth = DB::table('auth_rule')->where(['id'=>$v,'pid'=>0])->select('name','title','pid')->first();
			if($auth){
				$rule[] = $auth->title;
			}
		}

		return view('admin/index',['rule'=>$rule]);
	}
	
	// 基本信息
	public function website()
	{
		$info = array(
			'操作系统'=>PHP_OS,
			'运行环境'=>$_SERVER["SERVER_SOFTWARE"],
			'PHP运行方式'=>php_sapi_name(),
			'上传附件限制'=>ini_get('upload_max_filesize'),
			'执行时间限制'=>ini_get('max_execution_time').'秒',
			'服务器时间'=>date("Y年n月j日 H:i:s"),
			'北京时间'=>gmdate("Y年n月j日 H:i:s",time()+8*3600),
			'服务器域名/IP'=>$_SERVER['SERVER_NAME'].' [ '.gethostbyname($_SERVER['SERVER_NAME']).' ]',
			'剩余空间'=>round((disk_free_space(".")/(1024*1024)),2).'M',
			'register_globals'=>get_cfg_var("register_globals")=="1" ? "ON" : "OFF",
			'magic_quotes_gpc'=>(1===get_magic_quotes_gpc())?'YES':'NO',
			'magic_quotes_runtime'=>(1===get_magic_quotes_runtime())?'YES':'NO',
		);
		return view('admin/website',['info'=>$info]);
	}
	
	//清除缓存
	public function clear_cache(Request $request)
	{
		//清除所有的缓存
		$res = Cache::flush();
		if($res == true){
			return redirect('/prompt')->with(['message'=>'已清空所有缓存','url' =>'/admin/index', 'jumpTime'=>3,'status'=>'success']);
		} else {
			return redirect('/prompt')->with(['message'=>'没有任何缓存','url' =>'/admin/index', 'jumpTime'=>3,'status'=>'error']);
		}
	}
	
}
