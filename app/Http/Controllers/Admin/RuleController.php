<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class RuleController extends Controller
{
    // 权限列表
    public function rule_list()
    {
    	$rule_list = DB::table('auth_rule')->orderBy('id','asc')->get()->toArray();

    	return view('admin/rule_list',['rule'=>$this->search_rule($rule_list,0)]);
    }

    // 无限级分类查询
    public function search_rule($list,$fid)
    {
    	$arr = array();
	    foreach ($list as $key => $value) {
	        if ($value->pid == $fid) {
	            $value->child = $this->search_rule($list,$value->id);
	            $arr[] = $value;
	        }
	    }
	    return $arr;
    }

    // 添加权限
    public function rule_add(Request $request,$pid)
    {
    	if($request->isMethod('post')){
    		$name = $request->input('name');
    		$title = $request->input('title');
    		if(empty($name)) return redirect('/prompt')->with(['message'=>'请填写规则','url' =>'/admin/rule_add/'.$pid, 'jumpTime'=>3,'status'=>'error']);
    		if(empty($title)) return redirect('/prompt')->with(['message'=>'请填写名称','url' =>'/admin/rule_add/'.$pid, 'jumpTime'=>3,'status'=>'error']);
    		# 添加
    		$res = DB::table('auth_rule')->insert(['pid'=>$pid,'name'=>$name,'title'=>$title]);
    		if($res){
    			return redirect('/admin/rule_list');
    		} else {
    			return redirect('/prompt')->with(['message'=>'添加失败','url' =>'/admin/rule_add/'.$pid, 'jumpTime'=>3,'status'=>'error']);
    		}
    	}
    	return view('admin/rule_add',['pid'=>$pid]);
    }

    // 修改权限
    public function rule_edit(Request $request,$id)
    {
    	$rule = DB::table('auth_rule')->where(['id'=>$id])->select('name','id','title')->first();

    	if($request->isMethod('post')){
    		$name = $request->input('name');
    		$title = $request->input('title');
    		if(empty($name)) return redirect('/prompt')->with(['message'=>'请填写规则','url' =>'/admin/rule_edit/'.$id, 'jumpTime'=>3,'status'=>'error']);
    		if(empty($title)) return redirect('/prompt')->with(['message'=>'请填写名称','url' =>'/admin/rule_edit/'.$id, 'jumpTime'=>3,'status'=>'error']);
    		# 添加
    		$res = DB::table('auth_rule')->where(['id'=>$id])->update(['name'=>$name,'title'=>$title]);
    		if($res){
    			return redirect('/admin/rule_list');
    		} else {
    			return redirect('/prompt')->with(['message'=>'添加失败','url' =>'/admin/rule_edit/'.$id, 'jumpTime'=>3,'status'=>'error']);
    		}
    	}
    	return view('admin/rule_edit',['rule'=>$rule]);
    }

    // 删除权限
    public function rule_del(Request $request)
    {
    	# 判断删除的权限是否含有下级
    	$id = $request->input('id');
    	$rule = DB::table('auth_rule')->where(['pid'=>$id])->select('id')->first();
    	if($rule){
    		json_return(1000,'无法删除，含有下级权限');
    	} else {
    		# 直接删除
    		$res = DB::table('auth_rule')->where('id',$id)->delete();
    		json_return(200,'删除成功');
    	}
    }


    /*********************************管理组*******************************/
    public function auth_group()
    {
    	$auth_group = DB::table('auth_group')->select('title','id','bak','status')->get();
    	return view('admin/auth_group',['group'=>$auth_group]);
    }

    // 添加管理组
    public function group_add(Request $request)
    {
    	if($request->isMethod('post')){
    		$title = $request->input('title');
    		$bak = $request->input('bak') ?: '';
    		if(empty($title)) return redirect('/prompt')->with(['message'=>'请填写分组名','url' =>'/admin/group_add', 'jumpTime'=>3,'status'=>'error']);
    		# 插入数据
    		$res = DB::table('auth_group')->insert(['title'=>$title,'bak'=>$bak,'rules'=>'']);
    		return redirect('/admin/auth_group');
    	}
    	return view('admin/group_add');
    }

    // 修改分组
    public function group_edit(Request $request,$id)
    {
    	# 获取分组信息
    	$group = DB::table('auth_group')->where('id',$id)->select('id','title','bak','rules')->first();
    	$group->rules = explode(',', $group->rules);
    	# 获取权限列表
    	$rule_list  = DB::table('auth_rule')->where('status',1)->select('title','id','pid')->get();
    	$rule_list = $this->search_rule($rule_list,0);
    	# 提交修改
    	if($request->isMethod('post')){
    		$title = $request->input('title');
    		$bak = $request->input('bak') ?: '';
    		$rules = $request->input('rules') ?: '';
    		$ruels = implode(',', $rules);
    		# 修改
    		$res = DB::table('auth_group')->where('id',$id)->update(['title'=>$title,'bak'=>$bak,'rules'=>$ruels]);
    		if($res){
    			json_return(200,'修改成功');
    		} else {
    			json_return(1000,'修改失败');
    		}
    	}

    	return view('admin/group_edit',['group'=>$group,'rule_list'=>$rule_list]);
    }

    // 删除分组
    public function group_del(Request $request)
    {
    	$id = $request->input('id');
    	# 判断是否有相关组内人员
    	$has = DB::table('auth_group_access')->where('group_id',$id)->select('uid')->first();
    	if($has){
    		json_return(1000,'请先移除组内成员');
    	} else {
    		$res = DB::table('auth_group')->where('id',$id)->delete();
    		json_return(200,'删除成功');
    	}
    }

    /*********************************管理员列表*******************************/

    public function admin_user()
    {
    	$admin_list = DB::table('admin_user as a')->join('auth_group as b','a.group','=','b.id')->select('a.id','username','email','a.status','last_login_time','last_login_ip','group','b.title')->get();
    	return view('admin/admin_list',['admin_list'=>$admin_list]);
    }

    // 添加管理员
    public function admin_user_add(Request $request)
    {
    	# 获取用户组
    	$group = DB::table('auth_group')->where('status',1)->select('id','title')->get();
    	if($request->isMethod('post')){
    		$username = $request->input('username');
    		$password = $request->input('password');
    		$email = $request->input('email');
    		$group = (int)$request->input('group');

    		if(empty($username)) return redirect('/prompt')->with(['message'=>'请填写用户名','url' =>'/admin/admin_user_add', 'jumpTime'=>3,'status'=>'error']);
    		if(empty($password)) return redirect('/prompt')->with(['message'=>'请填写密码','url' =>'/admin/admin_user_add', 'jumpTime'=>3,'status'=>'error']);
    		# 插入数据
    		$res = DB::table('admin_user')->insertGetId(['username'=>$username,'password'=>md5($password),'email'=>$email,'group'=>$group]);
    		if($res){
                # 添加对应关系表
                DB::table('auth_group_access')->insert(['uid'=>$res,'group_id'=>$group]);
    			return redirect('/admin/admin_user');
    		} else {
    			return redirect('/prompt')->with(['message'=>'添加失败','url' =>'/admin/admin_user_add', 'jumpTime'=>3,'status'=>'error']);
    		}

    	}
    	return view('admin/admin_user_add',['group'=>$group]);
    }

    // 修改管理员
    public function admin_user_edit(Request $request,$id)
    {
    	# 获取用户组
    	$group = DB::table('auth_group')->where('status',1)->select('id','title')->get();
    	# 获取管理员信息
    	$admin = DB::table('admin_user')->where('id',$id)->select('username','password','email','group','id')->first();

    	# 修改
    	if($request->isMethod('post')){
			$username = $request->input('username');
    		$password = $request->input('password');
    		$email = $request->input('email');
    		$group = (int)$request->input('group');

    		if(empty($username)) return redirect('/prompt')->with(['message'=>'请填写用户名','url' =>'/admin/admin_user_add', 'jumpTime'=>3,'status'=>'error']);
    		# 修改
    		if(empty($password)){
    			$res = DB::table('admin_user')->where('id',$id)->update(['username'=>$username,'email'=>$email,'group'=>$group]);
    		} else {
    			$res = DB::table('admin_user')->where('id',$id)->update(['username'=>$username,'password'=>md5($password),'email'=>$email,'group'=>$group]);
    		}
    		
    		if($res){
                # 修改对应关系表
                DB::table('auth_group_access')->where('uid',$id)->update(['group_id'=>$group]);
    			return redirect('/admin/admin_user');
    		} else {
    			return redirect('/prompt')->with(['message'=>'修改失败','url' =>'/admin/admin_user_edit/'.$id, 'jumpTime'=>3,'status'=>'error']);
    		}
    	}

    	return view('admin/admin_user_edit',['group'=>$group,'admin'=>$admin]);
    }

    // 删除管理员
    public function admin_user_del(Request $request)
    {
    	$id = $request->input('id');
    	$res = DB::table('admin_user')->where('id',$id)->delete();
    	if($res){
    		json_return(200,'删除成功');
    	} else {
    		json_return(1000,'删除失败');
    	}
    }



}
