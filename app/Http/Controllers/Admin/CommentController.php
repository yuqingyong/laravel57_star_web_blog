<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CommentController extends Controller
{
    // 评论列表
	public function comment_list()
	{
		$comments = DB::table('comment')
				  ->join('article','article.aid','=','comment.aid')
				  ->join('users','users.uid','=','comment.uid')
				  ->select('comment.created_at','comment.contents','article.title','users.username','comment.status','cmtid')
				  ->orderBy('cmtid','desc')->paginate(20);
		return view('admin/comment_list',['comments'=>$comments]); 
	}
	
	// 禁用列表
	public function comment_status(Request $request)
	{
		if($request->isMethod('post')){
			$cmtid = $request->input('cmtid');
			$status = $request->input('status');
			if(!preg_match('/^[1-9]\d*$/',$cmtid)) json_return(1000,'参数错误');
			if(!preg_match('/^[0-1]\d*$/',$status)) json_return(1000,'参数错误');
			# 修改状态
			$res = DB::table('comment')->where('cmtid',$cmtid)->update(['status'=>$status]);
			if($res){
				json_return(200,'操作成功');
			} else {
				json_return(200,'操作失败');
			}
		} else {
			json_return(1000,'非法操作');
		}
	}
	
	// 删除列表
	public function comment_del(Request $request)
	{
		if($request->isMethod('post')){
			$cmtid = $request->input('cmtid');
			if(!preg_match('/^[1-9]\d*$/',$cmtid)) return redirect('/prompt')->with(['message'=>'参数错误','url' =>'/admin/comment_list', 'jumpTime'=>3,'status'=>'error']);
			
			# 删除数据
			$res = DB::table('comment')->where('cmtid',$cmtid)->delete();
			if($res){
				json_return(200,'删除成功');
			} else {
				json_return(1000,'删除失败');
			}
		}
	}
	
	
	
}
