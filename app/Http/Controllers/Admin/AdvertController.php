<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class AdvertController extends Controller
{
    // 广告列表
	public function advert_list()
	{
		$adverts = DB::table('advert')->orderBy('mid','desc')->paginate(20);
		return view('admin/advert_list',['adverts'=>$adverts]); 
	}
	
	// 禁用列表
	public function advert_status(Request $request)
	{
		if($request->isMethod('post')){
			$mid = $request->input('mid');
			$is_show = $request->input('is_show');
			if(!preg_match('/^[1-9]\d*$/',$mid)) json_return(1000,'参数错误');
			if(!preg_match('/^[0-1]\d*$/',$is_show)) json_return(1000,'参数错误');
			# 修改状态
			$res = DB::table('advert')->where('mid',$mid)->update(['is_show'=>$is_show]);
			if($res){
				json_return(200,'操作成功');
			} else {
				json_return(200,'操作失败');
			}
		} else {
			json_return(1000,'非法操作');
		}
	}
	
	// 添加列表
	public function advert_add(Request $request)
	{
		if($request->isMethod('post')){
            $result = $this->validate($request,[
                'mname' => 'required',
                'url' => 'required|url',
                'create_time' => 'required',
                'end_time' => 'required',
                'img' => 'required|image',
            ],[
                'mname' => '请输入链接名',
                'url' => '请输入正确的URl地址',
                'create_time' => '请选择开始时间',
                'end_time' => '请选择结束时间',
                'img' => ':img请上传图片',
            ]);

            $data['mname'] = $request->input('mname');
            $data['from'] = $request->input('from');
            $data['url'] = $request->input('url');
            $data['create_time'] = strtotime($request->input('create_time'));
            $data['end_time'] = strtotime($request->input('end_time'));
            $data['img'] = '/uploads/'.$request->file('img')->store(date('Ymd'));

            # 存储
            $res = DB::table('advert')->insert($data);
            if($res){
                return redirect('advert_list');
            } else {
                return redirect('/prompt')->with(['message'=>'添加失败，请联系客服','url' =>'/admin/advert_add', 'jumpTime'=>3,'status'=>'error']);
            }
		}
		return view('admin/advert_add');
	}
	
	// 修改列表
	public function advert_edit(Request $request,$mid)
	{
		if(!preg_match('/^[1-9]\d*$/',$mid)) return redirect('/prompt')->with(['message'=>'参数错误','url' =>'/admin/advert_list', 'jumpTime'=>3,'status'=>'error']);
		# 查询内容
		$advert = DB::table('advert')->where('mid',$mid)->select('img','mid','mname','url','create_time','end_time','from')->first();
		if($request->isMethod('post')){
            $result = $this->validate($request,[
                'mname' => 'required',
                'url' => 'required|url',
                'create_time' => 'required',
                'end_time' => 'required',
            ],[
                'mname' => '请输入链接名',
                'url' => '请输入正确的URl地址',
                'create_time' => '请选择开始时间',
                'end_time' => '请选择结束时间',
            ]);

            if($request->hasFile('img')){
                $result = $this->validate($request,[
                    'img' => 'required|image',
                ],[
                    'end_time' => ':img请上传图片',
                ]);
                $data['img'] = '/uploads/'.$request->file('img')->store(date('Ymd'));
            }

            $data['mname'] = $request->input('mname');
            $data['from'] = $request->input('from');
            $data['url'] = $request->input('url');
            $data['create_time'] = strtotime($request->input('create_time'));
            $data['end_time'] = strtotime($request->input('end_time'));

			# 更新数据
			$res = DB::table('advert')->where('mid',$mid)->update($data);
			if($res){
				return redirect('advert_list');
			} else {
				return redirect('/prompt')->with(['message'=>'更新失败，请联系客服','url' =>'/admin/advert_edit/'.$mid, 'jumpTime'=>3,'status'=>'error']);
			}
		}
		return view('admin/advert_edit',['advert'=>$advert]);
	}
	
	// 删除列表
	public function advert_del(Request $request)
	{
		if($request->isMethod('post')){
			$mid = $request->input('mid');
			if(!preg_match('/^[1-9]\d*$/',$mid)) return redirect('/prompt')->with(['message'=>'参数错误','url' =>'/admin/advert_list', 'jumpTime'=>3,'status'=>'error']);
			
			# 删除数据
            $path = DB::table('advert')->where('mid',$mid)->select('img')->first();
			if($path){
                DB::table('advert')->where('mid',$mid)->delete();
			    unlink('.'.$path->img);
				json_return(200,'删除成功');
			} else {
				json_return(1000,'删除失败');
			}
		}
	}
	
	
	
}
