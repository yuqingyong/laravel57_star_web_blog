<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class MessageController extends Controller
{
    // 留言列表
	public function message_list()
	{
		$messages = DB::table('message')->orderBy('id','desc')->paginate(20);
		return view('admin/message_list',['messages'=>$messages]); 
	}
	
	// 禁用列表
	public function message_status(Request $request)
	{
		if($request->isMethod('post')){
			$id = $request->input('id');
			$is_show = $request->input('is_show');
			if(!preg_match('/^[1-9]\d*$/',$id)) json_return(1000,'参数错误');
			if(!preg_match('/^[0-1]\d*$/',$is_show)) json_return(1000,'参数错误');
			# 修改状态
			$res = DB::table('message')->where('id',$id)->update(['is_show'=>$is_show]);
			if($res){
				json_return(200,'操作成功');
			} else {
				json_return(200,'操作失败');
			}
		} else {
			json_return(1000,'非法操作');
		}
	}
	
	// 删除列表
	public function message_del(Request $request)
	{
		if($request->isMethod('post')){
			$id = $request->input('id');
			if(!preg_match('/^[1-9]\d*$/',$id)) return redirect('/prompt')->with(['message'=>'参数错误','url' =>'/admin/message_list', 'jumpTime'=>3,'status'=>'error']);
			
			# 删除数据
			$res = DB::table('message')->where('id',$id)->delete();
			if($res){
				json_return(200,'删除成功');
			} else {
				json_return(1000,'删除失败');
			}
		}
	}
	
	
	
}
