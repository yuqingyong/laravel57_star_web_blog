<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\Advert;
use App\Models\Article;
use DB;
use session;

class IndexController extends Controller
{
    // 首页
    public function index(Request $request)
    {
    	# 获取页码
        $page = $request->input('page','1');
    	
    	# 获取banner
    	$advert_list = Advert::where('is_show',1)->select('url','img','mid','mname')->orderBy('mid','desc')->get();
    	
    	# 获取文章列表
    	if(Cache::has('article_list'.$page)){
            $article_list = Cache::get('article_list'.$page);
        } else {
            $article_list = Article::where('is_show',1)->select('aid','title','created_at','updated_at','cid','author','keywords','click','comment_num','description')->with('category','articlepic')->orderBy('sort','desc')->paginate(12);
            Cache::put('article_list'.$page,$article_list,3600*24);
        }

    	# 获取热门文章
    	if(Cache::has('hot_article')){
            $hot_article = Cache::get('hot_article');
        } else {
            $hot_article = Article::where('is_show',1)->select('aid','title','created_at','updated_at','cid','author','keywords','click','comment_num','description')->with('category','articlepic')->orderBy('click','desc')->limit(8)->get();
            Cache::put('hot_article',$hot_article,3600*24);
        }

    	# 获取友情链接
    	$link_list = DB::table('friendurl')->where('is_show',1)->select('lname','url','sort')->get();

    	# 获取网站统计数据
    	$article_count = Article::count();
    	$time = timediff(1493568000,time());
        
    	return view('home/index',['advert_list'=>$advert_list,'article_list'=>$article_list,'hot_article'=>$hot_article,'link_list'=>$link_list,'article_count'=>$article_count,'time'=>$time]);
    }
}
