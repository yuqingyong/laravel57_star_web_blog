<?php

namespace App\Http\Controllers\Home;

use App\Http\Requests\Api\MessageRequest;
use App\Http\Controllers\Controller;
use App\Models\Message;
use DB;

class MessageController extends Controller
{
	// 留言墙
	public function messageList()
	{
		# 获取列表
		$message_list = Message::where('is_show',1)->select('contents','nickname','create_time')->orderBy('id')->get();

		return view('home.Chat.messageList',['message_list'=>$message_list]);
	}

	// 提交留言
	public function sendMessage(MessageRequest $request)
	{
		$data['contents'] = htmlspecialchars($request->contents);
        $data['nickname'] = htmlspecialchars($request->nickname ?: '游客');
        $data['create_time'] = time();
        # 插入数据
        $res = Message::create($data);
        if($res){
        	return redirect('/home/messageList');
        } else {
        	return redirect('/prompt')->with(['message'=>'添加失败','url' =>'/home/messageList', 'jumpTime'=>3,'status'=>'error']);
        }
	}
}