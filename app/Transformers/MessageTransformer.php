<?php

namespace App\Transformers;

use App\Models\Message;
use League\Fractal\TransformerAbstract;

class MessageTransformer extends TransformerAbstract
{
    public function transform(Message $message)
    {
        return [
            'create_time' => date('Y-m-d H:i:s',$message->create_time),
            'contents' => $message->contents,
            'nickname' => $message->nickname,
        ];
    }



}