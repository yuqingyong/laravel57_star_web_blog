<?php

namespace App\Transformers;

use App\Models\ArticlePic;
use League\Fractal\TransformerAbstract;

class ArticlePicTransformer extends TransformerAbstract
{
    public function transform(ArticlePic $articlePic)
    {
        return [
            'path' => $articlePic->path,
            'aid' => $articlePic->aid,
        ];
    }



}

