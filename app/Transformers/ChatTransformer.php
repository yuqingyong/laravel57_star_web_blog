<?php

namespace App\Transformers;

use App\Models\Chat;
use League\Fractal\TransformerAbstract;

class ChatTransformer extends TransformerAbstract
{
    public function transform(Chat $chat)
    {
        return [
            'create_time' => date('Y-m-d H:i:s',$chat->create_time),
            'content' => $chat->content,
        ];
    }



}