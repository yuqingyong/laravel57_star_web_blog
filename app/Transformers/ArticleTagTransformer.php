<?php

namespace App\Transformers;

use App\Models\ArticleTag;
use League\Fractal\TransformerAbstract;

class ArticleTagTransformer extends TransformerAbstract
{
    # 嵌套资源
    protected $availableIncludes = ['articles','articlepic'];

    public function transform(ArticleTag $articletag)
    {
        return [
            'tid' => $articletag->tid,
            'aid' => $articletag->aid,
        ];
    }

    // 获取文章列表
    public function includeArticles(ArticleTag $articletag)
    {
        return $this->item($articletag->articles,new ArticleTransformer());
    }


}