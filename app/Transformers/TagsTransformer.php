<?php

namespace App\Transformers;

use App\Models\Tags;
use League\Fractal\TransformerAbstract;

class TagsTransformer extends TransformerAbstract
{
    public function transform(Tags $tags)
    {
        return [
            'tid' => $tags->tid,
            'tname' => $tags->tname,
        ];
    }



}