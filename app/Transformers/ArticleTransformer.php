<?php

namespace App\Transformers;

use App\Models\Article;
use League\Fractal\TransformerAbstract;

class ArticleTransformer extends TransformerAbstract
{
    # 嵌套资源
    protected $availableIncludes = ['category','articlepic'];

    public function transform(Article $article)
    {
        $data = [
            'aid' => $article->aid,
            'title' => $article->title,
            'cid' => $article->cid,
            'content' => $article->content,
            'author' => $article->author,
            'keywords' => $article->keywords,
            'is_show' => (int) $article->is_show,
            'sort' => (int) $article->sort,
            'click' => (int) $article->click,
            'comment_num' => $article->comment_num,
            'description' => $article->description,
            'created_at' => $article->created_at->toDateTimeString(),
            'updated_at' => $article->updated_at->toDateTimeString(),
            'deleted_at' => $article->deleted_at,
        ];

        if($article->content == null) unset($data['content']);
        if($article->deleted_at == null) unset($data['deleted_at']);
        return $data;
    }

    # 获取分类信息
    public function includeCategory(Article $article)
    {
        return $this->item($article->category, new CategoryTransformer());
    }

    # 获取文章封面
    public function includeArticlepic(Article $article)
    {
        return $this->item($article->articlepic, new ArticlePicTransformer());
    }


}