<?php

namespace App\Transformers;

use App\Models\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    public function transform(Category $category)
    {
        return [
            'cid' => $category->cid,
            'descripiton' => $category->descripiton,
            'keywords' => $category->keywords,
            'cname' => $category->cname,
        ];
    }



}