<?php

namespace App\Transformers;

use App\Models\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    # 关联数据
    protected $availableIncludes = ['headimg'];

    public function transform(Comment $comment)
    {
        return [
            'contents' => $comment->contents,
            'created_at' => date('Y-m-d H:i:s',$comment->created_at),
            'email' => $comment->email,
        ];
    }

    # 获取用户信息
    public function IncludeHeadimg(Comment $comment)
    {
        return $this->item($comment->headimg,new UserTransformer());
    }

}