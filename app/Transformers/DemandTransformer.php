<?php

namespace App\Transformers;

use App\Models\Demand;
use League\Fractal\TransformerAbstract;

class DemandTransformer extends TransformerAbstract
{
    # 关联数据
    protected $availableIncludes = ['headimg'];

    public function transform(Demand $demand)
    {
        $data = [
            'create_time' => date('Y-m-d H:i:s',$demand->create_time),
            'contents' => $demand->contents,
            'title' => $demand->title,
            'xid' => $demand->xid,
            'demand_type' => $demand->demand_type,
            'see_num' => $demand->see_num,
            'comment_num' => $demand->comment_num,
        ];
        if($demand->contents == null) unset($data['content']);
        return $data;
    }

    # 获取用户信息
    public function IncludeHeadimg(Demand $demand)
    {
        return $this->item($demand->headimg,new UserTransformer());
    }



}