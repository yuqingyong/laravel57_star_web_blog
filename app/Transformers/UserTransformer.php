<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $data = [
            'uid' => $user->uid,
            'username' => $user->username,
            'nickname' => $user->nickname,
            'head_img' => $user->head_img,
            'openid' => $user->openid,
            'created_at' => $user->created_at->toDateTimeString(),
            'updated_at' => $user->updated_at->toDateTimeString(),
            'email' => $user->email,
            'phone' => $user->phone,
        ];

        if($user->nickname == null) unset($data['nickname']);
        if($user->uid == null) unset($data['uid']);
        if($user->openid == null) unset($data['openid']);
        if($user->username == null) unset($data['username']);
        if($user->email == null) unset($data['email']);
        if($user->phone == null) unset($data['phone']);
        return $data;
    }



}