<?php

namespace App\Transformers;

use App\Models\Advert;
use League\Fractal\TransformerAbstract;

class AdvertTransformer extends TransformerAbstract
{
    public function transform(Advert $advert)
    {
        return [
            'mname' => $advert->mname,
            'from' => $advert->from,
            'img' => $advert->img,
            'url' => $advert->url,
            'type' => $advert->type,
            'create_time' => date('Y-m-d H:i:s',$advert->create_time),
            'end_time' => date('Y-m-d H:i:s',$advert->end_time),
        ];
    }



}