<?php

namespace App\Transformers;

use App\Models\Images;
use League\Fractal\TransformerAbstract;

class ImageTransformer extends TransformerAbstract
{
    public function transform(Images $image)
    {
        return [
            'id' => $image->id,
            'uid' => $image->uid,
            'type' => $image->type,
            'path' => $image->path,
            'created_at' => $image->created_at->toDateTimeString(),
            'updated_at' => $image->updated_at->toDateTimeString(),
        ];
    }
}