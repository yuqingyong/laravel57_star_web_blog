# laravel5.7星辰网络博客

#### 介绍
使用laravel5.7重新搭建的博客源码,包含api案例

#### 软件架构
该系统需要运行在php7.1版本以上，具体环境搭建可参考laravel官方手册


#### 安装教程

1. 安装步骤具体参照文档部署正式环境，需要注意的是需要给www用户写入权限，不然后台的文章图片无法上传
2. 在部署过程中我也遇到了不少问题，具体可参考这篇文章[星辰网络博客]:http://www.yuqingyong.cn/home/articleDetail/203

#### 使用说明

1. app/Http/Controllers/Admin  是后台部分，后台登录地址 http://127.0.0.1/yqyblog  admin 123123
2. app/Http/Controllers/Home   是前台部分，具体页面访问地址可查看路由文件  routes/web.php
3. app/Http/Controllers/Api    是接口开发部分，这部分只是用来做参考的，方便参考使用laravel5.7怎么开发一个接口的模式