<?php 
// timer定时器
class Server
{
	private $serv;

	public function __construct(){
		$this->serv = new swoole_server("0.0.0.0",9501);
		$this->serv->set(array(
			'worker_num' => 8,
			'daemonize' => false,
			'max_request' => 10000,
			'dispatch_mode' => 2,
		));

		$this->serv->on('Start', array($this,'onStart'));
		$this->serv->on('Connect',array($this,'onConnect'));
		$this->serv->on('Receive',array($this,'onReceive'));
		$this->serv->on('Close',array($this,'onClose'));
		$this->serv->on('WorkerStart', array($this,'onWorkerStart'));

		$this->serv->start();
	}

	public function onStart($serv){
		echo "Start\n";
	}

	public function onConnect($serv,$fd,$from_id){
		echo "Client {$fd} connect\n";
	}

	public function onClose($serv,$fd,$from_id){
		echo "Client {$fd} close connection\n";
	}

	public function onWorkerStart($serv,$worker_id){
		if($worker_id == 0){
			// 每一秒执行一次
			swoole_timer_tick(1000,function($timer_id,$params){
				echo "Timer running\n";
				echo "recv：{$params}\n";
			},"hello");
		}
	}

	public function onReceive(swoole_server $serv,$fd,$from_id,$data){
		echo "Get Message From client {$fd}:{$data}\n";
		// 每一秒之后执行一次
		swoole_timer_after(1000,function() use($serv,$fd){
			echo "Timer after\n";
			$serv->send($fd,'hello later timer\n');
		});

		echo "Continue Handle Worker\n";
	}

	public function onTick($timer_id,$params=null){
		echo "Timer {$timer_id} running\n";
		echo "Params：{$params}\n";
	}

}

$server = new Server();

