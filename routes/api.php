<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// 错误码
/**
 * 200 成功数据返回
 * 101 相应成功，但是缺少参数
 */

$api = app('Dingo\Api\Routing\Router');

$api->version('v1',['namespace' => 'App\Http\Controllers\Api','middleware' => 'serializer:array'],function($api){
    # 限制IP访问次数中间件
    $api->group(['middleware' => 'api.throttle','limit'=>config('api.rate_limits.access.limit'),'expires'=>config('api.rate_limits.access.expires')],function($api){
        //******************************** 游客可以访问的接口 *******************************//
        # 短信验证码
        $api->post('verificationCodes','VerificationCodesController@store')->name('api.verificationCodes.store');
        # 用户注册
        $api->post('users','UserController@store')->name('api.users.store');
        # 用户登录
        $api->post('authorizations','AuthorizationsController@store')->name('api.authorizations.store');
        # 刷新token
        $api->put('authorizations/update','AuthorizationsController@update')->name('api.authorizations.update');
        # 删除token
        $api->delete('authorizations/destroy','AuthorizationsController@destroy')->name('api.authorizations.destroy');
        # 图片验证码
        $api->post('captchas','CaptchasController@store')->name('api.captchas.store');

        # qq登录
        $api->get('qqlogin','QqController@qqlogin');
        # qq登录回调地址
        $api->get('qqnotify','QqController@qqnotify');

        # 根据条件获取文章列表
        $api->get('article_list','ArticleController@article_list')->name('api.article.article_list');
        # 获取文章详情
        $api->get('article_detail/{aid}','ArticleController@article_detail')->name('api.article.article_detail');
        # 获取标签列表
        $api->get('tag_list','TagController@tag_list')->name('api.tag.article_tag_list');
        # 根据标签获取文章列表
        $api->get('article_tag_list','TagController@article_tag_list')->name('api.tag.article_tag_list');
        # 获取banner图
        $api->get('advert_list','AdvertController@advert_list')->name('api.advert.advert_list');
        # 获取随笔
        $api->get('chat_list','ChatController@chat_list')->name('api.chat.chat_list');
        # 获取心愿墙数据
        $api->get('message_list','MessageController@message_list')->name('api.message.message_list');
        # 获取需求列表
        $api->get('demand_list','DemandController@demand_list')->name('api.demand.demand_list');

        //******************************** 需要token的接口 ********************************//
        // 需要在header头部设置token，设置格式：Bearer token（空格，然后获取的token）
        $api->group(['middleware' => 'api.auth'], function($api) {
            # 当前登录用户信息
            $api->get('userinfo', 'UserController@userinfo')->name('api.user.userinfo');
            # 图片资源
            $api->post('images','ImagesController@store')->name('api.images.store');
            # 用户评论
            $api->post('addcomment','CommentController@addcomment')->name('api.comment.addcomment');
            # 添加需求
            $api->post('add_demand','DemandController@add_demand')->name('api.demand.add_demand');
            # 添加留言
            $api->post('addmessage','MessageController@addmessage')->name('api.message.addmessage');
        });
    });
});
