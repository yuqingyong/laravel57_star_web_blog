<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// 跳转提示
Route::resource('/prompt','Admin\PromptController');

// 后台登录
Route::get('/yqylogin', function () {
    return view('admin.login');
});

// 后台登录验证
Route::post('/login','Admin\LoginController@login');

// 后台路由组
Route::group(['prefix'=>'admin'],function(){
    // 后台首页
    Route::get('/index','Admin\IndexController@show');
	Route::middleware(['AdminAuth'])->group(function () {
		// 退出登录
	    Route::get('/login_out','Admin\LoginController@login_out');
		// 清空缓存
	    Route::get('/clear_cache','Admin\IndexController@clear_cache');
	    // 基本信息
	    Route::get('/website','Admin\IndexController@website');
	    // 修改密码
	    Route::any('/edit_pass','Admin\LoginController@edit_pass');
		
		/**********************************会员管理************************************/
		
		// 会员列表
		Route::any('/member_list','Admin\MemberController@member_list');
		// 禁用会员
		Route::post('/member_status','Admin\MemberController@member_status');
		
		/**********************************闲言碎语************************************/
		
		// 闲言碎语列表
		Route::any('/chat_list','Admin\ChatController@chat_list');
		// 禁用列表
		Route::post('/chat_status','Admin\ChatController@chat_status');
		// 添加列表
		Route::any('/chat_add','Admin\ChatController@chat_add');
		// 修改列表
		Route::any('/chat_edit/{chid}','Admin\ChatController@chat_edit')->where(['chid'=>'[0-9]+']);
		// 删除列表
		Route::post('/chat_del','Admin\ChatController@chat_del');
		
		/**********************************用户评论************************************/
		
		// 用户评论列表
		Route::any('/comment_list','Admin\CommentController@comment_list');
		// 禁用评论
		Route::post('/comment_status','Admin\CommentController@comment_status');
		// 删除评论
		Route::post('/comment_del','Admin\CommentController@comment_del');
		
		/**********************************需求列表************************************/
		
		// 需求列表
		Route::any('/demand_list','Admin\DemandController@demand_list');
		// 禁用需求
		Route::post('/demand_status','Admin\DemandController@demand_status');
		// 删除需求
		Route::post('/demand_del','Admin\DemandController@demand_del');
		
		/**********************************留言列表************************************/
		
		// 留言列表
		Route::any('/message_list','Admin\MessageController@message_list');
		// 禁用留言
		Route::post('/message_status','Admin\MessageController@message_status');
		// 删除留言
		Route::post('/message_del','Admin\MessageController@message_del');
		
		/**********************************文章分类************************************/
		
		// 文章分类列表
		Route::any('/category_list','Admin\CategoryController@category_list');
		// 添加分类
		Route::any('/category_add','Admin\CategoryController@category_add');
		// 修改分类
		Route::any('/category_edit/{cid}','Admin\CategoryController@category_edit')->where(['cid'=>'[0-9]+']);
		// 删除分类
		Route::post('/category_del','Admin\CategoryController@category_del');
		
		/**********************************文章标签************************************/
		
		// 文章标签列表
		Route::any('/tag_list','Admin\TagController@tag_list');
		// 添加标签
		Route::any('/tag_add','Admin\TagController@tag_add');
		// 修改标签
		Route::any('/tag_edit/{tid}','Admin\TagController@tag_edit')->where(['tid'=>'[0-9]+']);
		// 删除标签
		Route::post('/tag_del','Admin\TagController@tag_del');
		
		/**********************************广告列表************************************/
		
		// 广告列表
		Route::any('/advert_list','Admin\AdvertController@advert_list');
		// 禁用广告
		Route::post('/advert_status','Admin\AdvertController@advert_status');
		// 添加广告
		Route::any('/advert_add','Admin\AdvertController@advert_add');
		// 修改广告
		Route::any('/advert_edit/{mid}','Admin\AdvertController@advert_edit')->where(['mid'=>'[0-9]+']);
		// 删除广告
		Route::post('/advert_del','Admin\AdvertController@advert_del');
		
		/**********************************友情链接************************************/
		
		// 友情链接列表
		Route::any('/friendurl_list','Admin\FriendurlController@friendurl_list');
		// 添加友情链接
		Route::any('/friendurl_add','Admin\FriendurlController@friendurl_add');
		// 修改友情链接
		Route::any('/friendurl_edit/{id}','Admin\FriendurlController@friendurl_edit')->where(['id'=>'[0-9]+']);
		// 删除友情链接
		Route::post('/friendurl_del','Admin\FriendurlController@friendurl_del');
		
		/**********************************文章管理************************************/
		
		// 文章列表
		Route::any('/article_list','Admin\ArticleController@article_list');
		// 添加文章
		Route::any('/article_add','Admin\ArticleController@article_add');
	    // 添加文章标签
	    Route::any('/add_article_tag','Admin\ArticleController@add_article_tag');
		// 文章显示状态
		Route::post('/article_status','Admin\ArticleController@article_status');
		// 修改文章
		Route::any('/article_edit/{aid}','Admin\ArticleController@article_edit')->where(['aid'=>'[0-9]+']);
		// 删除文章
		Route::post('/article_del','Admin\ArticleController@article_del');
	    // 回收站
	    Route::get('/article_recovery','Admin\ArticleController@article_recovery');
	    // 恢复文章
	    Route::post('/recovery','Admin\ArticleController@recovery');
	    // 彻底删除
	    Route::post('/del_all','Admin\ArticleController@del_all');

	    /**********************************权限管理************************************/

	    // 权限列表
	    Route::get('/rule_list','Admin\RuleController@rule_list');
		// 添加权限
		Route::any('/rule_add/{pid}','Admin\RuleController@rule_add')->where(['pid'=>'[0-9]+']);
		// 修改权限
		Route::any('/rule_edit/{id}','Admin\RuleController@rule_edit')->where(['id'=>'[0-9]+']);
		// 删除权限
		Route::post('/rule_del','Admin\RuleController@rule_del');
		// 管理员分组
		Route::get('/auth_group','Admin\RuleController@auth_group');
		// 添加分组
		Route::any('/group_add','Admin\RuleController@group_add');
		// 修改权限
		Route::any('/group_edit/{id}','Admin\RuleController@group_edit')->where(['id'=>'[0-9]+']);
		// 删除分组
		Route::post('/group_del','Admin\RuleController@group_del');
		// 管理员列表
		Route::get('/admin_user','Admin\RuleController@admin_user');
		// 添加管理员
		Route::any('/admin_user_add','Admin\RuleController@admin_user_add');
		// 修改管理员
		Route::any('/admin_user_edit/{id}','Admin\RuleController@admin_user_edit')->where(['id'=>'[0-9]+']);
		// 删除管理员
		Route::post('/admin_user_del','Admin\RuleController@admin_user_del');


	});
});

// 首页
Route::get('/','Home\IndexController@index');

// 前台路由组
Route::group(['prefix'=>'home'],function(){
	// 首页
	Route::get('/index','Home\IndexController@index');
	// 文章搜索
	Route::get('/articleSearch','Home\ArticleController@articleSearch');
	// 文章详情
	Route::get('/articleDetail/{aid}','Home\ArticleController@articleDetail')->where(['aid'=>'[0-9]+']);
	// 文章列表
	Route::get('/articleList/{cid}','Home\ArticleController@articleList')->where(['cid'=>'[2-7]']);
	// 源码分享
	Route::get('/codeShare','Home\ArticleController@codeShare');
	// 随心笔记
	Route::get('/chat','Home\ArticleController@chat');
	// 需求列表
	Route::get('/demandList','Home\DemandController@demandList');
	// 需求详情
	Route::get('/demandDetail/{xid}','Home\DemandController@demandDetail')->where(['xid'=>'[0-9]+']);
	// 留言墙
	Route::get('/messageList','Home\MessageController@messageList');
	// qq登录
	Route::any('/qqLogin','Home\LoginController@qqLogin');
	// qq登录回调
	Route::any('/qqLoginNotify','Home\LoginController@qqLoginNotify');
	// 退出登录
	Route::get('/loginOut','Home\LoginController@loginOut');

	// 登录验证组
	Route::middleware(['HomeAuth'])->group(function () {
		// 添加评论
		Route::post('/addComment','Home\ArticleController@addComment');
		// 发布需求页面
		Route::get('/demandRelease','Home\DemandController@demandRelease');
		// 提交需求操作
		Route::post('/sendDemand','Home\DemandController@sendDemand');
		// 提交留言
		Route::post('/sendMessage','Home\MessageController@sendMessage');
	});

});