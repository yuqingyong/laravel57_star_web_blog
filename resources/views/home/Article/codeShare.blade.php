@extends('home.layouts.app')
@section('title','源码分享')
@section('keywords','星辰网络博客，星辰，星辰网络，星辰博客，博客，PHP，thinkphp')
@section('description','一个个人的博客网站，有PHP，thinkphp，Linux等相关学习资料')
@section('content')
<style>
.git {margin-top:50px;text-align:right;}
.git span{margin-right: 10px;}
</style>
<section class="container">
  <div class="content-wrap">
    <div class="content">
      <div class="title">
        <h3 style="line-height: 1.3">源码分享</h3>
      </div>
        <article class="excerpt excerpt-1"><a class="focus" href="https://github.com/yuqingyong/yqyblog" title="源码分享" target="_blank" ><img class="thumb" data-original="/static/home/images/benzhanyuanma.png" src="/static/home/images/benzhanyuanma.png" alt="星辰网络博客站点源码分享"  style="display: inline;"></a>
          <header><a class="cat" href="https://gitee.com/yuqingyong/laravel57_star_web_blog.git" title="" >源码分享<i></i></a>
            <h2><a href="https://gitee.com/yuqingyong/laravel57_star_web_blog.git" title="星辰网络博客站点源码分享" target="_blank" >星辰网络博客站点源码分享</a></h2>
          </header>
            <p class="meta">
            <time class="time"><i class="glyphicon glyphicon-time"></i>2017-07-21</time>
            </p>
          <p class="note">本站使用了laravel5.7的框架开发，一个个人博客的网站系统，多次重构并整理网站代码....</p>
          <p class="git"><span><a href="https://gitee.com/yuqingyong/events">查看码云>></a></span><span><a href="https://gitee.com/yuqingyong/laravel57_star_web_blog.git">下载ZIP源码>></a></span></p>
        </article>

        <article class="excerpt excerpt-1"><a class="focus" href="https://github.com/yuqingyong/laravel" title="源码分享" target="_blank" ><img class="thumb" data-original="/static/home/images/benzhanyuanma.png" src="/static/home/images/benzhanyuanma.png" alt="vue-demo项目源码分享"  style="display: inline;"></a>
          <header><a class="cat" href="https://github.com/yuqingyong/laravel" title="" >源码分享<i></i></a>
            <h2><a href="https://github.com/yuqingyong/laravel" title="vue-demo项目源码分享" target="_blank" >Vuex项目demo</a></h2>
          </header>
            <p class="meta">
            <time class="time"><i class="glyphicon glyphicon-time"></i>2017-07-21</time>
            </p>
          <p class="note">vue使用脚手架加vue加本站前端页面的demo....</p>
          <p class="git"><span><a href="https://github.com/yuqingyong/vue-demo">查看GitHub>></a></span><span><a href="https://github.com/yuqingyong/vue-demo/archive/master.zip">下载ZIP源码>></a></span></p>
        </article>
    </div>
  </div>
  <aside class="sidebar">
    <div class="fixed">
      <div class="widget widget_search">
        <form class="navbar-form" action="{{ url('home/articleSearch') }}" method="get">
          <div class="input-group">
            <input type="text" name="keyword" class="form-control" size="35" placeholder="请输入关键字" maxlength="15" autocomplete="off">
            <span class="input-group-btn">
            <button class="btn btn-default btn-search" type="submit">搜索</button>
            </span> </div>
        </form>
      </div>
      <div class="widget widget_sentence">
        <h3>标签云</h3>
        <div class="widget-sentence-content">
            <ul class="plinks ptags">
              @foreach($article_tag as $tag)
                <li><a href="/home/articleSearch?tid={{$tag->tid}}" title="{{ $tag->tname }}" draggable="false">{{ $tag->tname }}</a></li> 
              @endforeach
            </ul>
        </div>
      </div>
    </div>
    <div class="widget widget_hot">
          <h3>最热文章</h3>
          <ul>
            @foreach($hot_article as $va)
                <li><a title="{{ $va->title }}" href="{{ url('home/Articles/detail',array('aid'=>$va->aid)) }}" ><span class="thumbnail">
                    <img class="thumb" data-original="{{ $va->articlepic->path }}" src="{{ $va->articlepic->path }}" alt="{{ $va->title }}"  style="display: block;">
                </span><span class="text">{{ $va->title }}</span><span class="muted"><i class="glyphicon glyphicon-time"></i>
                    {{ $va->created_at}}
                </span><span class="muted"><i class="glyphicon glyphicon-eye-open"></i>{{ $va->click }}</span></a></li>
            @endforeach
          </ul>
      </div>
      @include('home.public.rili')
      <div class="widget widget_sentence">    
        @include('home.public.music')
      </div>
  </aside>
</section>
@endsection