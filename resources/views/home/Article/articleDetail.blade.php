@extends('home.layouts.app')
@section('title',$detail->title)
@section('keywords',$detail->title)
@section('description',$detail->description)
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
    .header_img{width: 50px;height: 50px;float: left;margin-right: 10px;}
    .comment-main p{padding: 5px;}
  </style>
  <section class="container">
  <div class="content-wrap">
    <div class="content" style="background:#fff;padding: 10px;">
      <header class="article-header">
        <h1 class="article-title"><a title="{{$detail->title}}" >{{$detail->title}}</a></h1>
        <div class="article-meta"> <span class="item article-meta-time">
          <time class="time" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="发表时间：{{$detail->created_at}}"><i class="glyphicon glyphicon-time"></i> {{$detail->created_at}}</time>
          </span> <span class="item article-meta-source" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="来源：{{$detail->author}}"><i class="glyphicon glyphicon-globe"></i> {{$detail->author}}</span> <span class="item article-meta-category" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="MZ-NetBlog主题"><i class="glyphicon glyphicon-list"></i>
                  @switch($detail->cid)
                      @case(2)
                        技术分享
                        @break
                      @case(3)
                        源码分享
                        @break
                      @case(4)
                        随心笔记
                        @break
                      @case(5)
                        推荐文章
                        @break
                      @case(7)
                        资讯分享
                        @break
                    @endswitch
                  </a></span> <span class="item article-meta-views" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="浏览量：{{$detail->click}}"><i class="glyphicon glyphicon-eye-open"></i> {{$detail->click}}</span> <span class="item article-meta-comment" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="评论量"><i class="glyphicon glyphicon-comment"></i> {{$detail->comment_num}}</span> </div>
      </header>
      <article class="article-content" style="padding: 10px;">
          {!! $detail->content !!}
      </article>
      <div class="relates" style="background:#fbfbfb;padding: 5px;">
        <div class="title">
          <h3>相关推荐</h3>
        </div>
        <ul>
          @foreach($relation_article as $rel)
              <li><a href="{{ url('home/articleDetail',array('aid'=>$rel->aid)) }}" title="" >{{$rel->title}}</a></li>
          @endforeach
        </ul>
      </div>
      <div class="title" id="comment">
        <h3>评论</h3>
      </div>
      <div id="respond">
            <div class="comment">
                <div class="comment-box">
                    <textarea placeholder="您的评论或留言（必填）" class="user_input" name="content" id="comment-textarea" cols="100%" rows="3" tabindex="3"></textarea>
                    <div class="comment-ctrl">
                        <i style="font-size: 30px;padding: 5px;color: #ccc;float: left;" class="fa fa-smile-o" id="emoji_tip"></i>
                        <input style="height: 30px;float: left;margin-top: 5px;float: left;width: 185px;" class="form-control b-email" id="email" type="text" name="email" placeholder="接收回复的email地址" value="">
                        <button type="button" name="comment-submit" id="comment-submit" tabindex="4" onclick="add_comment({{ $detail->aid }} )">评论</button>
                    </div>
                </div>
            </div>
            <!-- 表情 -->
            <div class="emoji_div" style="display: none;"></div>
        </div>
      <div id="postcomments">
        <ol id="comment_list" class="commentlist">
          <!-- <li class="comment-content">
            <img src="/static/home/images/default_head_img.gif" class="header_img">
            <span class="comment-f">2018-05-16</span>
            <div class="comment-main"><p><a class="address" rel="nofollow" target="_blank">yuqingyong</a><br>dkalsjdklasjdlkasjldkasj</p></div>
          </li> -->
		    </ol>
      </div>
    </div>
  </div>
  <aside class="sidebar">
    <div class="fixed">
      <div class="widget widget-tabs">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#notice" aria-controls="notice" role="tab" data-toggle="tab" >统计信息</a></li>
          <li role="presentation"><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab" >联系站长</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane contact active" id="notice">
            <h2>日志总数:{{ $article_count }}篇
              </h2>
              <h2>网站运行:
              <span id="sitetime">{{ $time }}天 </span></h2>
          </div>
            <div role="tabpanel" class="tab-pane contact" id="contact">
              <h2>QQ:
                  <a href="http://wpa.qq.com/msgrd?v=3&amp;uin=1425219094&amp;site=qq&amp;menu=yes" target="_blank" rel="nofollow" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="QQ:1425219094">1425219094</a>
              </h2>
              <h2>Email:
              <a href="mailto:1425219094@qq.com" target="_blank" data-toggle="tooltip" rel="nofollow" data-placement="bottom" title=""  data-original-title="Email:1425219094@qq.com">1425219094@qq.com</a></h2>
          </div>
        </div>
      </div>
      <div class="widget widget_search">
        <form class="navbar-form" action="{{ url('home/articleSearch') }}" method="get">
          <div class="input-group">
            <input type="text" name="keyword" class="form-control" size="35" placeholder="请输入关键字" maxlength="15" autocomplete="off">
            <span class="input-group-btn">
            <button class="btn btn-default btn-search" type="submit">搜索</button>
            </span> </div>
        </form>
      </div>
    </div>
    <div class="widget widget_hot">
          <h3>最热文章</h3>
          <ul>     
            @foreach($hot_article as $va)
                <li><a title="{{ $va->title }}" href="{{ url('home/articleDetail',array('aid'=>$va->aid)) }}" ><span class="thumbnail">
                    <img class="thumb" data-original="{{ $va->articlepic->path }}" src="{{ $va->articlepic->path }}" alt="{{ $va->title }}"  style="display: block;">
                </span><span class="text">{{ $va->title }}</span><span class="muted"><i class="glyphicon glyphicon-time"></i>
                    {{ $va->created_at}}
                </span><span class="muted"><i class="glyphicon glyphicon-eye-open"></i>{{ $va->click }}</span></a></li>
            @endforeach
          </ul>
     </div>
     <!--日历插件开始-->
        @include('home.public.rili')
     <!--日历插件结束-->
     <div class="widget widget_sentence">    
        @include('home.public.music')
     </div>
  </aside>
</section>
<textarea style="display: none;" id="comment_data">{{ $comment }}</textarea>
</body>
@if (count($errors) > 0)
   <script>
    var msg = "{{ $errors->first() }}";
    layer.msg(msg);
   </script>
@endif
<script>
  $(document).on('click',function(){
    $(".emoji_div").fadeOut();
  })
  
  var comment = JSON.parse($("#comment_data").val());

  //评论显示模板
  var tpl = "<li class='comment-content'><img src='{header}' class='header_img'><span class='comment-f'>{time}</span><div class='comment-main'><p><a class='address' rel='nofollow' target='_blank'>{name}</a><br>{content}</p></div></li>";

  show_comment();
  // 输入框对象
  var user_input = document.getElementsByClassName('user_input')[0];
  

  $('#emoji_tip').on('click', function(e) {
      e.stopPropagation();
      if ($('.emoji_div').find('.emoji').size() == 0) {
          show_biaoqing();
      }
      $('.emoji_div').fadeIn();
  });

  // 显示表情
  function show_biaoqing() {
      var row = 5, col = 15;
      var str = '<table class="emoji">';
      for (var i = 0; i < row; i++) {
          str += '<tr>';
          for (var j = 0; j < col; j++) {
              var n = i * col + j;
              str += '<td>' + (n > 71 ? '' : ('<img onclick="select_emoji(' + n + ');" src="/static/home/face/' + n + '.gif" />')) + '</td>';
          }
          str += '</tr>';
      }
      str += '</table>';
      $('.emoji_div').html(str);
  }

  // 选择表情
  function select_emoji(n) {
      cursor_insert(user_input, '{@' + n + '}');
      $(".emoji_div").fadeOut();
  }

  // 光标处插入内容
  function cursor_insert(obj, txt) {
      if (document.selection) {
          obj.selection.createRange().text = txt;
      } else {
          var v = obj.value;
          var i = obj.selectionStart;
          obj.value = v.substr(0, i) + txt + v.substr(i);
          user_input.focus();
          obj.selectionStart = i + txt.length;
      }
  }

  // 解析消息中的表情
  function analysis_emoji(str) {
      var p = /{@(\d|[1-6]\d|7[01])}/;
      if (p.test(str)) {
          return analysis_emoji(str.replace(p, "<img src='/static/home/face/$1.gif'/>"))
      } else {
          return str;
      }
  }

  //显示评论
  function show_comment(){
    var str = '';
    for(var i in comment){
      var data = comment[i];
      str += tpl.replace('{header}',data.head_img != 0 ? data.head_img : '/static/home/images/default_head_img.gif').replace('{time}',data.created_at).replace('{name}',data.username).replace('{content}',analysis_emoji(data.contents));
    }
    $("#comment_list").html(str);
  }

  //添加评论
  function add_comment(aid){
    // 输入框默认焦点
    user_input.focus();
    var content = $("#comment-textarea").val();
    var email = $("#email").val();
    // var preg = '/^[a-zA-Z0-9_-]+@([a-zA-Z0-9]+\.)+(com|cn|net|org)$/';
    if(content == ''){
    	alert('评论的内容或者邮箱不能为空！');
    }else if(!email.match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/)){
      alert('邮箱格式不正确');
    }else{
    	$.ajax({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
	       url:"{{ url('home/addComment') }}",
	       type:'post',
	       data:{'aid':aid,'contents':content,'email':email},
	       success:function(date){
	          if(date.status == 1){
	            setTimeout(function() {
	                location.reload();
	            }, 1000);
	          }else if (JSON.parse(date).status == 2000) {
              alert(JSON.parse(date).msg)
            } else{
	            alert(date.msg)
	            setTimeout(function() {
	                location.reload();
	            }, 2000);
	          }
	       }
	     })
    }
  }
</script>
@endsection
