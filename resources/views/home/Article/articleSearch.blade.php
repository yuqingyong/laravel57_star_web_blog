@extends('home.layouts.app')
@section('title','搜索页')
@section('keywords','星辰网络博客，星辰，星辰网络，星辰博客，博客，PHP，thinkphp')
@section('description','一个个人的博客网站，有PHP，thinkphp，Linux等相关学习资料')
@section('content')
  <section class="container">
  <div class="content-wrap">
    <div class="content">
      <div class="title">
        <h3 style="line-height: 1.3">查询关键字：{{ $keyword }}</h3>
      </div>
      @empty($article_list)
      <article class="excerpt excerpt-1">暂时没有相关文章！</article>
      @endempty

      @foreach($article_list as $article)
      <article class="excerpt excerpt-1" style="">
      <a class="focus" href="{{ url('home/articleDetail',array('aid'=>$article->aid)) }}" title="{{ $article->title }}" target="_blank" ><img class="thumb" data-original="{{ $article->articlepic->path }}" src="{{ $article->articlepic->path }}" alt="{{ $article->keywords }}"  style="display: inline;"></a>
            <header><a class="cat" href="{{ url('home/articleDetail',array('aid'=>$article->aid)) }}" title="{{ $article->cid }}" >
                    @switch($article->cid)
                      @case(2)
                        技术分享
                        @break
                      @case(3)
                        源码分享
                        @break
                      @case(4)
                        随心笔记
                        @break
                      @case(5)
                        推荐文章
                        @break
                      @case(7)
                        资讯分享
                        @break
                    @endswitch
                    <i></i></a>
                <h2><a href="{{ url('home/articleDetail',array('aid'=>$article->aid)) }}" title="{{ $article->title }}" target="_blank" >{{ $article->title }}</a>
                </h2>
            </header>
            <p class="meta">
                <time class="time"><i class="glyphicon glyphicon-time"></i>{{ $article->created_at }}</time>
                <span class="views"><i class="glyphicon glyphicon-eye-open"></i> {{ $article->click }}</span> <a class="comment" href="#" title="评论" target="_blank" ><i class="glyphicon glyphicon-comment"></i>{{ $article->comment_num }}</a>
            </p>
            <p class="note">{{ $article->description }}</p>
        </article>
       @endforeach
    </div>
  </div>
  <aside class="sidebar">
    <div class="fixed">
      <div class="widget widget_search">
        <form class="navbar-form" action="{{ url('home/articleSearch') }}" method="get">
          <div class="input-group">
            <input type="text" name="keyword" class="form-control" size="35" placeholder="请输入关键字" maxlength="15" autocomplete="off">
            <span class="input-group-btn">
            <button class="btn btn-default btn-search" type="submit">搜索</button>
            </span> </div>
        </form>
      </div>
      <div class="widget widget_sentence">
        <h3>标签云</h3>
        <div class="widget-sentence-content">
            <ul class="plinks ptags">                
                @foreach($article_tag as $tag)
                <li><a href="/home/articleSearch?tid={{$tag->tid}}" title="{{ $tag->tname }}" draggable="false">{{ $tag->tname }}</a></li> 
                @endforeach             
            </ul>
        </div>
      </div>
    </div>
    <div class="widget widget_hot">
          <h3>最热文章</h3>
          <ul>
            @foreach($hot_article as $va)
                <li><a title="{{ $va->title }}" href="{{ url('home/articleDetail',array('aid'=>$va->aid)) }}" ><span class="thumbnail">
                    <img class="thumb" data-original="{{ $va->articlepic->path }}" src="{{ $va->articlepic->path }}" alt="{{ $va->title }}"  style="display: block;">
                </span><span class="text">{{ $va->title }}</span><span class="muted"><i class="glyphicon glyphicon-time"></i>
                    {{ $va->created_at}}
                </span><span class="muted"><i class="glyphicon glyphicon-eye-open"></i>{{ $va->click }}</span></a></li>
            @endforeach
          </ul>
      </div>
     @include('home.public.rili')
     <div class="widget widget_sentence">    
        @include('home.public.music')
     </div>
  </aside>
</section>
@endsection
