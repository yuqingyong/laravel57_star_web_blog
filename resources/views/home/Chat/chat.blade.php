<!DOCTYPE html>
<html lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>随心语录</title>
	<meta name="keywords" content="星辰网络博客，星辰，星辰网络，星辰博客，博客，PHP，thinkphp">
    <meta name="description" content="一个个人的博客网站，有PHP，thinkphp，Linux等相关学习资料">
	<meta http-equiv="Cache-Control" content="no-siteapp">
	<meta name="author" content="www.myweb.cn">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="/static/home/bjy/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/static/home/bjy/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="/static/home/bjy/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/static/home/bjy/bjy.css">
	<link rel="stylesheet" type="text/css" href="/static/home/bjy/index.css">
	<link rel="stylesheet" type="text/css" href="/static/home/bjy/animate.css">
	<link rel="stylesheet" type="text/css" href="/static/home/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/static/home/css/nprogress.css">
    <link rel="stylesheet" type="text/css" href="/static/home/css/style.css">
    <link rel="stylesheet" type="text/css" href="/static/home/css/font-awesome.min.css">
    <link rel="apple-touch-icon-precomposed" href="/static/home/images/icon.png">
    <!-- <link rel="shortcut icon" href="images/favicon.ico"> -->
    <script src="/static/home/js/jquery-2.1.4.min.js"></script>
    <script src="/static/home/js/layer.js"></script>
	</head>

<body class=" pace-done">
<header class="header">
  <nav class="navbar navbar-default" id="navbar">
    <div class="container">
      <div class="header-topbar hidden-xs link-border">
        <ul class="site-nav topmenu">
          @if(Session::has('users'))
            <li><a href="#}">欢迎您：{{ Session::get('users.username') }}</a></li>
            <li><a href="{{ url('home/loginOut') }}">退出</a></li>
          @else
              <li>
                <a  href="{{ url('home/qqLogin') }}" title="QQ登录" ><img style="width: 25px;" src="http://www.geren-jianli.net/upload/2/86/28678da89bf0de60f7341597e6943703.jpg">QQ登录</a>
              </li>
          @endif
        </ul> 勤记录 懂分享</div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar" aria-expanded="false"> <span class="sr-only"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <h1 class="logo hvr-bounce-in"><a href="/" title="星辰网络博客"><img src="/static/home/images/logo.png" alt="星辰网络博客"></a></h1>
      </div>
      <div class="collapse navbar-collapse" id="header-navbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a data-cont="星辰网络博客" title="星辰网络博客" href="/">首页</a></li>
          <li><a data-cont="最新资讯" title="最新资讯" href="/home/articleList/7">最新资讯</a></li>
          <li><a data-cont="IT技术笔记" title="IT技术笔记" href="/home/articleList/2">IT技术笔记</a></li>
          <li><a data-cont="源码分享" title="404" href="{{ url('home/codeShare') }}">源码分享</a></li>
          <li><a data-cont="随心笔记" title="随心笔记题" href="{{ url('home/chat') }}" >随心笔记</a></li>
          <li><a data-cont="需求发布" title="需求发布" href="{{ url('home/demandList') }}" >需求发布</a></li>
          <li><a data-cont="心语心愿" title="心语心愿" href="{{ url('home/messageList') }}" >心语心愿</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
		<div id="b-content" class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-lg-8 b-chat">
					<div class="b-chat-left">
						@foreach($chat_list as $k=>$v)
						@if($k%2 == 1)
						<ul class="b-chat-one animated bounceInLeft">
							<li class="b-chat-title ">{{ $v->create_time }}</li>
							<li class="b-chat-content">{{ $v->content }}</li>
							<div class="b-arrows-right1" style="top: 28.8px;">
								<div class="b-arrows-round"></div>
							</div>
							<div class="b-arrows-right2" style="top: 28.8px;"></div>
						</ul>
						@endif
						@endforeach
					</div>
					<div class="b-chat-middle" style="height: 100%;"></div>
					<div class="b-chat-right">
						@foreach($chat_list as $k=>$v)
						@if($k%2 == 0)
						<ul class="b-chat-one animated bounceInRight">
							<li class="b-chat-title ">{{ $v->create_time }}</li>
							<li class="b-chat-content">{{ $v->content }}</li>
							<div class="b-arrows-right1" style="top: 45.6px;">
								<div class="b-arrows-round"></div>
							</div>
							<div class="b-arrows-right2" style="top: 45.6px;"></div>
						</ul>
						@endif
						@endforeach
					</div>
				</div>
				<div id="b-public-right" class="col-lg-4 hidden-xs hidden-sm hidden-md">
					<div class="widget widget_sentence">
				        <h3>标签云</h3>
				        <div class="widget-sentence-content">
				            <ul class="plinks ptags">       
				            	@foreach($article_tag as $tag)
					                <li><a href="/home/articleSearch?tid={{$tag->tid}}" title="{{ $tag->tname }}" draggable="false">{{ $tag->tname }}</a></li> 
					            @endforeach
				            </ul>
				        </div>
				      </div>
					<div class="b-recommend">
						<h4 class="b-title">最热文章</h4>
						<p class="b-recommend-p">
							@foreach($hot_article as $va)
				                <a class="b-recommend-a" href="{{ url('home/articleDetail',array('aid'=>$va->aid)) }}" target="_blank"><span class="fa fa-th-list b-black"></span> {{ $va->title }}</a>
				            @endforeach
						</p>
					</div>
					<div class="b-search">
						<form class="form-inline" role="form" action="{{ url('home/articleDetail') }}" method="get"> <input class="b-search-text" type="text" name="keyword"> <input class="b-search-submit" type="submit" value="全站搜索"></form>
					</div>
				</div>
			</div>
			
		</div>
		<script src="/static/home/bjy/hm.js"></script>
		<script src="/static/home/bjy/push.js"></script>
		<script src="/static/home/bjy/jquery-2.0.0.min.js"></script>
		<script src="/static/home/bjy/bootstrap.min.js"></script>
		<script src="/static/home/bjy/pace.min.js"></script>
		<script src="/static/home/bjy/index.js"></script>
		<!-- 百度页面自动提交开始 -->
		<script>
			(function() {
				var bp = document.createElement('script');
				var curProtocol = window.location.protocol.split(':')[0];
				if(curProtocol === 'https') {
					bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
				} else {
					bp.src = 'http://push.zhanzhang.baidu.com/push.js';
				}
				var s = document.getElementsByTagName("script")[0];
				s.parentNode.insertBefore(bp, s);
			})();
		</script>
		<!-- 百度页面自动提交结束 -->

		<!-- 百度统计开始 -->
		<script>
			var _hmt = _hmt || [];
			(function() {
				var hm = document.createElement("script");
				hm.src = "//hm.baidu.com/hm.js?c3338ec467285d953aba86d9bd01cd93";
				var s = document.getElementsByTagName("script")[0];
				s.parentNode.insertBefore(hm, s);
			})();
		</script>
		<script>
			function login(){
				var txt = "<a href='http://www.baidu.com'><img src='/static/home/bjy/qq-login.png'></a>";
				layer.open({
				  type: 1,
				  skin: 'layui-layer-rim', //加上边框
				  area: ['420px', '240px'], //宽高
				  content: txt
				});
			}
		</script>
		<!-- 百度统计结束 -->
	</body>

</html>