<html>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="/static/home/liuyan/base.css" type="text/css"/>
        <link rel="stylesheet" href="/static/home/liuyan/tan.css" type="text/css"/>
        
        <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.js"></script>
        <script src="/static/home/liuyan/main.js"></script>
        <script src="/static/home/liuyan/index.js" type="text/javascript"></script>
        <script src="/static/home/js/layer.js"></script>
        <title>心语墙</title>
    </head>
    <body>
        <div class="main">
            <div class="main_nav">
            	<ul>
            		<li><a class="cd-signin" href="#0">写一句话</a></li>
					<li><a  href="/">返回首页</a></li>
            	</ul>
            </div>
            @foreach($message_list as $key=>$val)
            @if($key % 2 == 0)
            <div name="note" class="note">
                <div class="nhead" style="background-image: url(/static/home/liuyan/a5_1.gif);">
                    {{ date('Y-m-d H:i:s',$val->create_time) }}
                </div>
                <div class="nbody" style="background-image: url(/static/home/liuyan/a5_2.gif);">
                     {{ $val->contents }}
                </div>
                <div class="nfoot" style="background-image: url(/static/home/liuyan/a5_3.gif);">
                    <div class="moodpic">
                        <img src="/static/home/liuyan/17.gif"/>
                    </div>
                    <div class="username">
                        {{ $val->nickname }}
                    </div>
                </div>
            </div>
            @else
            <div name="note" class="note">
                <div class="nhead" style="background-image: url(/static/home/liuyan/a1_1.gif);">
                    {{ date('Y-m-d H:i:s',$val->create_time) }}
                </div>
                <div class="nbody" style="background-image: url(/static/home/liuyan/a1_2.gif);">
                     {{ $val->contents }}
                </div>
                <div class="nfoot" style="background-image: url(/static/home/liuyan/a1_3.gif);">
                    <div class="moodpic">
                        <img src="/static/home/liuyan/3.gif"/>
                    </div>
                    <div class="username">
                        {{ $val->nickname }}
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>

<!--弹出表单开始-->
<div class="cd-user-modal" style="z-index:99999"> 
<div class="cd-user-modal-container">
	<ul class="cd-switcher" style="padding:0">
		<li><a href="#0">写下心中想说的话</a></li>
	</ul>
	<div id="cd-login"> <!-- 登录表单 -->
        
		<form class="cd-form" action="{{ url('home/sendMessage') }}" method="post">
            @csrf
			<p class="fieldset">
				<textarea type="text" maxlength="100"  name="contents" style="width:530px;height:100px;" placeholder="请输入你想说的话（最多100个字符）"/></textarea>
			</p>

			<p class="fieldset">
				<input class="full-width has-padding has-border" id="signin-password" name="nickname" type="text" maxlength="5" placeholder="留名(默认为：游客)，最多5个字符">
			</p>

			<p class="fieldset">
				<input class="full-width2" type="submit" value="提 交">
			</p>
		</form>
	</div>
	<a href="#0" class="cd-close-form">关闭</a>
</div>
</div>
<!--弹出表单结束-->
@if (count($errors) > 0)
   <script>
    var msg = "{{ $errors->first() }}";
    layer.msg(msg);
   </script>
@endif
</body>
</html>