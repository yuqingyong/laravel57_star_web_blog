@extends('home.layouts.app')
@section('title','首页')
@section('keywords','星辰网络博客，星辰，星辰网络，星辰博客，博客，PHP，thinkphp')
@section('description','一个个人的博客网站，有PHP，thinkphp，Linux等相关学习资料')
@section('content')
<section class="container">
  <div class="content-wrap">
    <div class="content">
        
        <div id="focusslide" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach($advert_list as $key => $advert)
                <li data-target="#focusslide" data-slide-to="{{$key}}" class="active"></li>
            @endforeach
        </ol>
        <div class="carousel-inner" role="listbox">
          @foreach($advert_list as $key => $advert)
            @if($key == 1)
            <div class="item active">
            <a href="{{$advert->url}}" target="_blank" title="{{$advert->mname}}" >
            <img src="{{$advert->img}}" alt="{{$advert->mname}}" class="img-responsive"></a>
            </div>
            @else
             <div class="item">
            <a href="{{$advert->url}}" target="_blank" title="{{$advert->mname}}" >
            <img src="{{$advert->img}}" alt="{{$advert->mname}}" class="img-responsive"></a>
            </div>
            @endif
          @endforeach
        </div>
        <a class="left carousel-control" href="#focusslide" role="button" data-slide="prev" rel="nofollow"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">上一个</span> </a> <a class="right carousel-control" href="#focusslide" role="button" data-slide="next" rel="nofollow"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">下一个</span> </a> </div>
        <article class="excerpt-minic excerpt-minic-index">
            <h2><span class="red">【公告】</span><a target="_blank" href="/" title="" >本站源码下载</a>
            </h2>
            <p class="note">本站的源码可供免费下载，下载地址是：https://gitee.com/yuqingyong/laravel57_star_web_blog.git，无论是转载任何文章，都无需打招呼！</p>
        </article>
      <div class="title">
        <h3>最新发布</h3>
      </div>
      @foreach($article_list as $article)
      <article class="excerpt excerpt-1" style="">
      <a class="focus" href="{{ url('home/articleDetail',array('aid'=>$article->aid)) }}" title="{{ $article->title }}" target="_blank" ><img class="thumb" data-original="{{ $article->articlepic->path }}" src="{{ $article->articlepic->path }}" alt="{{ $article->keywords }}"  style="display: inline;"></a>
            <header><a class="cat" href="{{ url('home/articleDetail',array('aid'=>$article->aid)) }}" title="{{ $article->cid }}" >
                    @switch($article->cid)
                      @case(2)
                        技术分享
                        @break
                      @case(3)
                        源码分享
                        @break
                      @case(4)
                        随心笔记
                        @break
                      @case(5)
                        推荐文章
                        @break
                      @case(7)
                        资讯分享
                        @break
                    @endswitch
                    <i></i></a>
                <h2><a href="{{ url('home/articleDetail',array('aid'=>$article->aid)) }}" title="{{ $article->title }}" target="_blank" >{{ $article->title }}</a>
                </h2>
            </header>
            <p class="meta">
                <time class="time"><i class="glyphicon glyphicon-time"></i>{{ $article->created_at }}</time>
                <span class="views"><i class="glyphicon glyphicon-eye-open"></i> {{ $article->click }}</span> <a class="comment" href="#" title="评论" target="_blank" ><i class="glyphicon glyphicon-comment"></i>{{ $article->comment_num }}</a>
            </p>
            <p class="note">{{ $article->description }}</p>
        </article>
      @endforeach

      <nav class="pagination">
        <ul>
          {{ $article_list->links() }}
        </ul>
      </nav>
    </div>
  </div>
  <aside class="sidebar">
    <div class="fixed">
      <div class="widget widget-tabs">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#notice" aria-controls="notice" role="tab" data-toggle="tab" >统计信息</a></li>
          <li role="presentation"><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab" >联系站长</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane contact active" id="notice">
            <h2>日志总数:{{$article_count}}篇
              </h2>
              <h2>网站运行:
              <span id="sitetime">{{$time}}天</span></h2>
          </div>
            <div role="tabpanel" class="tab-pane contact" id="contact">
              <h2>QQ:
                  <a href="http://wpa.qq.com/msgrd?v=3&amp;uin=1425219094&amp;site=qq&amp;menu=yes" target="_blank" rel="nofollow" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="QQ:1425219094">1425219094</a>
              </h2>
              <h2>Email:
              <a href="http://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=1425219094@qq.com" target="_blank" data-toggle="tooltip" rel="nofollow" data-placement="bottom" title=""  data-original-title="Email:1425219094@qq.com">1425219094@qq.com</a></h2>
          </div>
        </div>
      </div>
      <div class="widget widget_search">
        <form class="navbar-form" action="{{ url('home/articleSearch') }}" method="get">
          <div class="input-group">
            <input type="text" name="keyword" class="form-control" size="35" placeholder="请输入关键字" maxlength="15" autocomplete="off">
            <span class="input-group-btn">
            <button class="btn btn-default btn-search" type="submit">搜索</button>
            </span> </div>
        </form>
      </div>
    </div>
    <div class="widget widget_hot">
          <h3>最热文章</h3>
          <ul>     
            @foreach($hot_article as $va)
                <li><a title="{{ $va->title }}" href="{{ url('home/articleDetail',array('aid'=>$va->aid)) }}" ><span class="thumbnail">
                    <img class="thumb" data-original="{{ $va->articlepic->path }}" src="{{ $va->articlepic->path }}" alt="{{ $va->title }}"  style="display: block;">
                </span><span class="text">{{ $va->title }}</span><span class="muted"><i class="glyphicon glyphicon-time"></i>
                    {{ $va->created_at}}
                </span><span class="muted"><i class="glyphicon glyphicon-eye-open"></i>{{ $va->click }}</span></a></li>
            @endforeach
          </ul>
     </div>
     <!--日历插件开始-->
        @include('home.public.rili')
     <!--日历插件结束-->
     <div class="widget widget_sentence">    
        @include('home.public.music')
     </div>
     <div class="widget widget_sentence">
      <h3>友情链接</h3>
      <div class="widget-sentence-link">
        @foreach($link_list as $link)
        <a href="{{ $link->url }}" title="{{ $link->lname }}" target="_blank" >{{ $link->lname }}</a>&nbsp;&nbsp;&nbsp;
        @endforeach
      </div>
    </div>
  </aside>
</section>
@endsection