<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>发布需求 - 星辰网络博客</title>
	<meta name="keywords" content="星辰网络博客，星辰，星辰网络，星辰博客，博客，PHP，thinkphp">
    <meta name="description" content="一个个人的博客网站，有PHP，thinkphp，Linux等相关学习资料">
	<link rel="alternate" type="application/rss+xml" title="星辰网络博客" href="/">
	<link href="http://www.thinkphp.cn/Public/favicon.ico" rel="shortcut icon">
	<link rel="stylesheet" type="text/css" href="/static/home/css/base.css" media="all">
	<link rel="stylesheet" type="text/css" href="/static/home/css/header.css" media="all">
	<link rel="stylesheet" type="text/css" href="/static/home/css/module.css" media="all">
	<script charset="utf-8" src="/static/admin/kindeditor/kindeditor.js"></script>
	<script charset="utf-8" src="/static/admin/kindeditor/lang/zh-CN.js"></script>
	</head>
	<style>
		#release{width: 100%;height: 25px;margin: 20px;}
		#release a{text-decoration: none;color: #0160A0;font-size: 28px;}
	</style>
	<script>
            KindEditor.ready(function(K) {
                    window.editor = K.create('#editor_id');
            });
            var options = {
                    cssPath : '/css/index.css',
                    filterMode : true
            };
            var editor = K.create('textarea[name="content"]', options);
            html = editor.html();
            // 同步数据后可以直接取得textarea的value
            editor.sync();
            html = document.getElementById('editor_id').value; // 原生API
            html = K('#editor_id').val(); // KindEditor Node API
            html = $('#editor_id').val(); // jQuery
            // 设置HTML内容
            editor.html('HTML内容');
            // 关闭过滤模式，保留所有标签
            // KindEditor.options.filterMode = false;

            // KindEditor.ready(function(K)) {
            //         K.create('#editor_id');
            // }
    </script>
	<body>
		<div id="release"><a href="{{ url('home/demandList') }}">返回需求列表</a></div>
		<div class="main cf">
			<!-- 左边发布表单 -->
			<div class="wrapper">
				<form id="form" action="{{ url('home/sendDemand') }}" method="post" class="think-form" enctype="multipart/form-data">
					@csrf
					<!-- 头部用户信息 -->
					<div class="hd">
						<div class="avatar"><img src="/static/home/images/80_80.gif" alt=""></div>
						<div class="hd-info">
							<strong>用户一</strong>
							<span class="time"><?php echo date('Y-m-d',time()) ?></span>
						</div>
						<div class="hd-title">发布新需求</div>
					</div>
					<!-- /头部用户信息 -->

					<!-- 表单项 -->

					<table class="bd">
						<tbody>
							<tr>
								<th><i class="must">*</i>标题</th>
								<td class="cols-in"><input class="text" type="text" name="title" datatype="*1-50" nullmsg="标题不能为空" errormsg="长度太长"></td>
								<td><span class="Validform_checktip"></span></td>
							</tr>
							<tr>
								<th><i class="must">*</i>分类</th>
								<td>
									<select id="cate" name="demand_type" datatype="*" nullmsg="分类不能为空">
										<option value="项目承接">项目承接</option>
										<option value="项目招募">项目招募</option>
										<option value="人才招聘">人才招聘</option>
									</select>
								</td>
								<td><span class="Validform_checktip"></span></td>
							</tr>
							<tr>
								<th><i class="must">*</i>内容</th>
								<td colspan="2">
									<div class="think-editor">
										<div class="enter">
											<textarea id="editor_id" name="contents" style="height:300px;"></textarea>
										</div>
									</div>

								</td>
							</tr>
							<tr>
								<th>&nbsp;</th>
								<td colspan="2">
									<input class="submit" type="submit" value="提交">
									<span id="error_msg_show"></span>
								</td>
							</tr>
						</tbody>
					</table>

					<!-- /表单项 -->
				</form>
			</div>
			<!-- /左边发布表单 -->

			<!-- 边栏 -->
			<div class="sidebar">

				<div class="box key">
					<div class="hd" style="">发布需求</div>
					<ul class="bd">
						<li>请完整输入需求的标题和内容，并选择恰当的分类；</li>
						<li>根据自己的要求发布相应的内容，请勿发布恶意需求，谢谢！</li>
					</ul>
				</div>

				<!-- 快捷键小贴士 -->
				<div class="box key">
					<div class="hd">最新发布</div>
					<div class="bd">
						<div>
							@foreach($demand_list as $demand)
							<a style="display: block;" href="{{ url('home/demandDetail',['xid'=>$demand->xid]) }}">{{ substr($demand->title,0,16) }}<span>&nbsp;&nbsp;&nbsp;&nbsp;{{ date('Y-m-d',$demand->create_time) }}</span></a>
							@endforeach
						</div>
					</div>
				</div>
				<!-- /快捷键小贴士 -->
			</div>
			<!-- /边栏 -->
		</div>
		@if (count($errors) > 0)
		   <script>
		    var msg = "{{ $errors->first() }}";
		    alert(msg);
		   </script>
		@endif
	</body>
</html>