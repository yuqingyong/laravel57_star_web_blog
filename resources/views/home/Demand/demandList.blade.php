@extends('home.layouts.app')
@section('title','需求列表')
@section('keywords','需求列表，查看需求，发布需求')
@section('description','一个个人的博客网站，有PHP，thinkphp，Linux等相关学习资料')
@section('content')
<style>
.demand{
	width: 150px;
	height: 100px;
	background: #3399CC;
	text-align: center;
	line-height: 100px;
}
.demand a {
	font-weight:bold;
	font-size: 24px;
	color: #fff;
}
</style>
<section class="container">
  <div class="content-wrap">
    <div class="content">
      <div class="title">
        <h3 style="line-height: 1.3">需求列表</h3>
      </div>
      @empty($demand_list)
      <article class="excerpt excerpt-1"><a href="{{ url('home/demandRelease') }}">暂时没有相关需求！前往发布需求>></a></article>
      @endempty
      @foreach($demand_list as $demand)
        <article class="excerpt excerpt-1">
        	<a class="focus" href="{{ url('home/demandDetail',array('xid'=>$demand->xid)) }}" title="{{ $demand->title }}" target="_blank" >
            @switch($demand->demand_type)
              @case('项目承接')
                <img class="thumb" data-original="/static/home/images/chenjie.jpg" src="/static/home/images/chenjie.jpg" alt="项目承接"  style="display: inline;">
                @break
              @case('人才招聘')
                <img class="thumb" data-original="/static/home/images/rencai.jpg" src="/static/home/images/rencai.jpg" alt="人才招聘"  style="display: inline;">
                @break
              @case('项目招募')
                <img class="thumb" data-original="/static/home/images/zhaomu.jpg" src="/static/home/images/zhaomu.jpg" alt="项目招募"  style="display: inline;">
                @break
              @default
                <img class="thumb" data-original="/static/home/images/chenjie.jpg" src="/static/home/images/chenjie.jpg" alt="项目承接"  style="display: inline;">
                @break
            @endswitch
        	</a>
        <header><a class="cat" href="{{ url('home/demandDetail',array('xid'=>$demand->xid)) }}">需求列表<i></i></a>
          <h2><a href="{{ url('home/demandDetail',array('xid'=>$demand->xid)) }}" target="_blank" >{{ $demand->title }}</a></h2>
        </header>
        <p class="meta">
        <time class="time"><i class="glyphicon glyphicon-time"></i>{{ date('Y-m-d H:i',$demand->create_time) }}</time>
        <span class="views"><i class="glyphicon glyphicon-eye-open"></i>{{ $demand->see_num }}</span> <a class="comment"  title="评论" target="_blank" ><i class="glyphicon glyphicon-comment"></i>{{ $demand->comment_num }}</a></p>
        <p class="note"><?php echo substr($demand->contents,0,280); ?> </p>
      </article>
      @endforeach
      <nav class="pagination" style="display: none;">
        <ul>
          {{ $demand_list->links() }}
        </ul>
      </nav>
        
    </div>
  </div>
  <aside class="sidebar">
    <div class="fixed">
      <div class="widget widget_sentence">
        <h3>发布需求</h3>
        <div class="widget-sentence-content">
            	<li class="demand"><a href="{{ url('home/demandRelease') }}">发布需求>></a></li>
        </div>
      </div>
    </div>
    <div class="widget widget_hot">
          <h3>待布局开发</h3>
          <ul>
            
          </ul>
      </div>
      @include('home.public.rili')
     <div class="widget widget_sentence">    
        @include('home.public.music')
      </div>
  </aside>
</section>
@endsection
