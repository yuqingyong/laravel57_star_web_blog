@extends('home.layouts.app')
@section('title', $detail->title)
@section('keywords','需求列表，查看需求，发布需求')
@section('description','一个个人的博客网站，有PHP，thinkphp，Linux等相关学习资料')
@section('content')
<section class="container">
	<div class="content-wrap">
		<div class="content" style="background:#fff;padding: 10px;">
			<div style="text-align: right;"><a href="mailto:{{ $userinfo->email }}">私信：{{ $userinfo->username }}</a></div>
			<header class="article-header">
				<h1 class="article-title"><a  title="{{ $detail->title }}" >{{ $detail->title }}</a></h1>
				<div class="article-meta"> <span class="item article-meta-time">
		          <time class="time" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="发表时间：{$art_detail.create_time}"><i class="glyphicon glyphicon-time"></i> {{ date('Y-m-d H:i',$detail->create_time) }}</time>
		          </span> <span class="item article-meta-source" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="来源：本站"><i class="glyphicon glyphicon-globe"></i> {{ $detail->demand_type }}</span> <span class="item article-meta-category" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="MZ-NetBlog主题"><i class="glyphicon glyphicon-list"></i>
                  需求详情</a></span> <span class="item article-meta-views" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="浏览量：{$art_detail.see_num}"><i class="glyphicon glyphicon-eye-open"></i> {{ $detail->see_num }}</span> <span class="item article-meta-comment" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="评论量"><i class="glyphicon glyphicon-comment"></i> {{ $detail->comment_num }}</span> </div>
			</header>
			<article class="article-content">
				{!! $detail->contents !!}
			</article>
		</div>
	</div>
	<aside class="sidebar">
		<div class="fixed">
			<div class="widget widget-tabs">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<a href="#contact" aria-controls="contact" role="tab" data-toggle="tab" draggable="false">联系站长</a>
					</li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane contact" id="contact" style="display: block;">
						<h2>QQ:
                  		<a href="http://wpa.qq.com/msgrd?v=3&amp;uin=1425219094&amp;site=qq&amp;menu=yes" target="_blank" rel="nofollow" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="QQ:1425219094">1425219094</a>
              			</h2>
              			<h2>Email:
              				<a href="mailto:1425219094@qq.com" target="_blank" data-toggle="tooltip" rel="nofollow" data-placement="bottom" title=""  data-original-title="Email:1425219094@qq.com">1425219094@qq.com</a>
              			</h2>
					</div>
				</div>
			</div>
			<div class="widget widget_search">
				<form class="navbar-form" action="{{ url('home/articleSearch') }}" method="get">
		          <div class="input-group">
		            <input type="text" name="keyword" class="form-control" size="35" placeholder="请输入关键字" maxlength="15" autocomplete="off">
		            <span class="input-group-btn">
		            <button class="btn btn-default btn-search" type="submit">搜索</button>
		            </span> </div>
		        </form>
			</div>
		</div>
		<div class="widget widget_hot">
			<h3>最热文章</h3>
			<ul>
				@foreach($hot_article as $va)
                <li><a title="{{ $va->title }}" href="{{ url('home/articleDetail',array('aid'=>$va->aid)) }}" ><span class="thumbnail">
                    <img class="thumb" data-original="{{ $va->articlepic->path }}" src="{{ $va->articlepic->path }}" alt="{{ $va->title }}"  style="display: block;">
                </span><span class="text">{{ $va->title }}</span><span class="muted"><i class="glyphicon glyphicon-time"></i>
                    {{ $va->created_at}}
                </span><span class="muted"><i class="glyphicon glyphicon-eye-open"></i>{{ $va->click }}</span></a></li>
            	@endforeach
			</ul>
		</div>
		@include('home.public.rili')
     	<div class="widget widget_sentence">    
        @include('home.public.music')
	    </div>
	</aside>
</section>
@endsection