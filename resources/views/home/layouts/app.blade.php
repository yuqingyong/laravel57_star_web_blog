<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>星辰网络博客 - @yield('title')</title>
    <meta name="keywords" content="@yield('keywords')">
    <meta name="description" content="@yield('description')">
    <link rel="stylesheet" type="text/css" href="/static/home/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/static/home/css/nprogress.css">
    <link rel="stylesheet" type="text/css" href="/static/home/css/style.css">
    <link rel="stylesheet" type="text/css" href="/static/home/css/prettyprint.css">
    <link rel="stylesheet" type="text/css" href="/static/home/css/font-awesome.min.css">
    <link rel="shortcut icon" href="/favicon.ico">
    <script src="/static/home/js/jquery-2.1.4.min.js"></script>
  	<script src="/static/home/js/layer.js"></script>
    <!--[if gte IE 9]>
      <script src="/static/home/js/jquery-1.11.1.min.js" type="text/javascript"></script>
      <script src="/static/home/js/html5shiv.min.js" type="text/javascript"></script>
      <script src="/static/home/js/respond.min.js" type="text/javascript"></script>
      <script src="/static/home/js/selectivizr-min.js" type="text/javascript"></script>
    <![endif]-->
    <!--[if lt IE 9]>
      <script>window.location.href='upgrade-browser.html';</script>
    <![endif]-->
</head>
<body>
<header class="header">
  <nav class="navbar navbar-default" id="navbar">
    <div class="container">
      <div class="header-topbar hidden-xs link-border">
        <ul class="site-nav topmenu">
          @if(Session::has('users'))
            <li><a href="#}">欢迎您：{{ Session::get('users.username') }}</a></li>
            <li><a href="{{ url('home/loginOut') }}">退出</a></li>
          @else
              <li>
                <a  href="{{ url('home/qqLogin') }}" title="QQ登录" ><img style="width: 25px;" src="http://www.geren-jianli.net/upload/2/86/28678da89bf0de60f7341597e6943703.jpg">QQ登录</a>
              </li>
          @endif
        </ul> 勤记录 懂分享</div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar" aria-expanded="false"> <span class="sr-only"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <h1 class="logo hvr-bounce-in"><a href="/" title="星辰网络博客"><img src="/static/home/images/logo.png" alt="星辰网络博客"></a></h1>
      </div>
      <div class="collapse navbar-collapse" id="header-navbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a data-cont="星辰网络博客" title="星辰网络博客" href="/">首页</a></li>
          <li><a data-cont="最新资讯" title="最新资讯" href="/home/articleList/7">最新资讯</a></li>
          <li><a data-cont="IT技术笔记" title="IT技术笔记" href="/home/articleList/2">IT技术笔记</a></li>
          <li><a data-cont="源码分享" title="404" href="{{ url('home/codeShare') }}">源码分享</a></li>
          <li><a data-cont="随心笔记" title="随心笔记题" href="{{ url('home/chat') }}" >随心笔记</a></li>
          <li><a data-cont="需求发布" title="需求发布" href="{{ url('home/demandList') }}" >需求发布</a></li>
          <li><a data-cont="心语心愿" title="心语心愿" href="{{ url('home/messageList') }}" >心语心愿</a></li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<script>
  $(function() {
    window.prettyPrint();
  })
</script>

@yield('content')

<footer class="footer">
  <div class="container">
    <p>本站[<a href="http://www.muzhuangnet.com/" >星辰网络博客</a>]的部分内容来源于网络，若侵犯到您的利益，请联系站长删除！谢谢！Powered By [<a href="/" target="_blank" rel="nofollow" >DTcms</a>] Version 4.0 &nbsp;<a href="/" target="_blank" rel="nofollow" >赣ICP备17010344</a> &nbsp; <a href="/" target="_blank" class="sitemap" >网站地图</a></p>
  </div>
  <div id="gotop"><a class="gotop"></a></div>
</footer>
 <script src="/static/home/js/bootstrap.min.js"></script>
<script src="/static/home/js/jquery.ias.js"></script>
<script src="/static/home/js/prettyprint.js"></script>
</body>
</html>