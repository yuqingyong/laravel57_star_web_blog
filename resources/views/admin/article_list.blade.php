<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="/static/admin/css/pintuer.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">
<script src="/static/admin/js/jquery.js"></script>
<script src="/static/admin/js/pintuer.js"></script>
<script src="/static/admin/js/layer.js"></script>
</head>
<style>
 .content{
 	width: 100%;
 	height: 100%;
 	padding-left: 10px;
 	padding-top: 10px;
 }
 .content li{
 	width: 100px;
 	height: 30px;
 	float: left;
 }
 .btn{
 	text-align: center;
 	width: 100%;
 	height: 50px;
 	float: left;
 } 
 .top{
 	width: 100%;
 	height: 30px;
 	font-family: "微软雅黑",
 	font-size:14px;
 	font-weight: bold;
 }
</style>
<body>
<form method="post" action="{{ url('admin/article_search') }}" id="listform">
   <div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 文章列表</strong></div>
    <div class="padding border-bottom">
      <ul class="search" style="padding-left:10px;">
        <li> <a class="button border-main icon-plus-square-o" href="{{ url('admin/article_add') }}"> 添加文章</a> </li>
        <li>搜索：</li>
        <li>
          <input type="text" placeholder="请输入搜索关键字" name="keyword" class="input" style="width:250px; line-height:17px;display:inline-block" />
          <input type="submit" value="搜索" class="button border-main icon-search"/>
        </li>
      </ul>
    </div>
    <div class="panel-head"><strong class="icon-reorder"> 文章列表</strong></div>
    
    <table class="table table-hover text-center">
      <tr>
        <th width="100" style="text-align:left; padding-left:20px;">ID</th>
        <th>封面</th>
        <th>标题</th>
        <th>发布时间</th>
        <th>作者</th>
        <th>排序</th>
        <th>点击量</th>
        <th>是否展示</th>
        <th>操作</th>
      </tr>
			@foreach ($articles as $article)
        <tr>
          <td style="text-align:left; padding-left:20px;"><input type="checkbox" name="id[]" value="" />{{ $article->aid }}</td>
          <td><img src="{{ $article->path }}" alt="" style="width:100px;height: 90px;"></td>
          <td>{{ $article->title }}</td>
          <td>{{ date("Y-m-d H:i:s",$article->created_at) }}</td>
          <td>{{ $article->author }}</td>
          <td>{{ $article->sort }}</td>
          <td>{{ $article->click }}</td>
					@switch($article->is_show)
						@case(1)
							<td onclick="is_set({{ $article->aid }},'best',0)">是</td>
							@break
						@case(0)
							<td style="color: red;" onclick="is_set({{ $article->aid }},'best',1)">否</td>
							@break
					@endswitch
          <td>
          	<div class="button-group">
          		<a class="button border-main" href="javascript:void(0)" onclick="return tag_select({{ $article->aid }})"><span class="icon-edit"></span> 添加标签</a>
          		@if ($article->is_show == 0)
          		<a class="button border-main" href="javascript:void(0)" onclick="return is_show({{ $article->aid }},1)"><span class="icon-edit"></span> 启用</a>
          		@else
          		<a class="button border-main" href="javascript:void(0)" onclick="return is_show({{ $article->aid }},0)"><span class="icon-edit"></span> 禁用</a>
          		@endif
          		<a class="button border-main" href="{{ url('admin/article_edit',['aid'=>$article->aid]) }}"><span class="icon-edit"></span> 修改</a>
          		<a class="button border-red" href="javascript:void(0)" onclick="return del({{ $article->aid }})"><span class="icon-trash-o"></span> 删除</a> 
          	</div>
          </td>
        </tr>
			@endforeach
    </table>
    <div class="page">
    	{{ $articles->links() }}
    </div>
  </div>
</form>
<script type="text/javascript">
//删除
function del(aid){
	//询问框
	layer.confirm('您确定要删除该文章吗?', {
	  btn: ['确定','取消'] //按钮
	}, function(){
	    $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},	
			type:"post",
			url:"{{ url('admin/article_del') }}",
			data:{'aid':aid},
			success:function(msg){
				if(msg.err_code == 200){
					location.reload();
				}else{
					layer.msg(msg.err_msg);
					location.reload();
				}
			}
		});
	}, function(){
	   layer.msg('已取消');
	});

}

//是否显示
function is_show(aid,is_show)
{

	layer.confirm('您确定要修改状态?', {
	  btn: ['确定','取消'] //按钮
	}, function(){
	    $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},	
			type:"post",
			url:"{{ url('admin/article_status') }}",
			data:{'aid':aid,'is_show':is_show},
			success:function(msg){
				if(msg.err_code == 200){
					location.reload();
				}else{
					layer.msg(msg.err_msg);
					location.reload();
				}
			}
		});
	}, function(){
	   layer.msg('已取消');
	});
	
	
}

//弹出标签选择层
function tag_select(aid)
{
	var arr = <?php echo json_encode($tags); ?>;
    var tpl = "<li><input type='checkbox' value='{tid}' name='tid[]'>{tname}</li>";
    var str_h = "<div class='btn'><input type='submit' value='提交' ></div>";
    var str_t = "<div class='top'>请选择好需要添加的标签</div>";
    var str_t = "<input type='hidden' name='aid' value="+aid+">";
    var csrf = "<input type='hidden' name='_token' value='{{ csrf_token() }}'>";
    layer.open({
        type: 1,
        skin: 'layui-layer-rim', //加上边框
        area: ['450px', '300px'], //宽高
        content: "<form action='{{ url('admin/add_article_tag') }}' method='post'><div id='content' class='content'></div></form>"
    });
	var str = '';
	for (var i = 0,c=arr.length; i < c; i++) {
		var list = arr[i];
    	str += tpl.replace('{tid}', list.tid).replace('{tname}', list.tname);
	}
	$("#content").html(str_t+str+str_h+csrf);
}
</script>
</body>
</html>