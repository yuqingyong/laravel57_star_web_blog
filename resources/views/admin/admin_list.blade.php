<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title></title>
<link rel="stylesheet" href="/static/admin/css/pintuer.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">
<script src="/static/admin/js/jquery.js"></script>
<script src="/static/admin/js/pintuer.js"></script>
<script src="/static/admin/js/layer.js"></script>
</head>
<body>
<form id="listform">
   <div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 管理员列表</strong></div>
    <div class="padding border-bottom">
      <ul class="search" style="padding-left:10px;">
        <li> <a class="button border-main icon-plus-square-o" style="cursor: pointer;" href="{{ url('admin/admin_user_add') }}"> 添加管理员</a> </li>
      </ul>
    </div>
    
    <table class="table table-hover text-center">
      <tr>
        <th width="100" style="text-align:left; padding-left:20px;">ID</th>
        <th>用户名</th>
        <th>邮箱</th>
        <th>上次登录时间</th>
        <th>上次登录IP</th>
        <th>所在用户组</th>
        <th>状态</th>
        <th width="310">操作</th>
      </tr>
		@foreach ($admin_list as $va)
        <tr>
          <td style="text-align:left; padding-left:20px;"><input type="checkbox" name="id[]" value="" />{{ $va->id }}</td>
          <td>{{ $va->username }}</td>
          <td>{{ $va->email }}</td>
          <td>{{ date('Y-m-d H:i:s',$va->last_login_time) }}</td>
          <td>{{ $va->last_login_ip }}</td>
          <td>{{ $va->title }}</td>
          <td>
          	@switch($va->status)
          		@case(1) 启用 @break
          		@case(0) 禁用 @break
          	@endswitch
          </td>
          <td>
          	<div class="button-group">
          		<a class="button border-main" style="cursor: pointer;" href="{{ url('admin/admin_user_edit',['id'=>$va->id]) }}"><span class="icon-edit"></span> 修改</a>
          		<a class="button border-red" href="javascript:void(0)" onclick="del({{ $va->id }})"><span class="icon-trash-o"></span> 删除</a> 
          	</div>
          </td>
        </tr>
		@endforeach
    </table>
  </div>
</form>

<script type="text/javascript">

//删除
function del(id){
	//询问框
	layer.confirm('您确定要删除该管理员吗?', {
	  btn: ['确定','取消'] //按钮
	}, function(){
	    $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type:"post",
			url:"{{ url('admin/admin_user_del') }}",
			data:{'id':id},
			success:function(msg){
				if(msg.err_code == 200){
					location.reload();
				}else{
					layer.msg(msg.err_msg);
					location.reload();
				}
			}
		});
	}, function(){
	   layer.msg('已取消');
	});

}
</script>
</body>
</html>