<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="/static/admin/css/pintuer.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="/static/admin/js/jquery.js"></script>
<script src="/static/admin/js/pintuer.js"></script>
<script src="/static/admin/js/layer.js"></script>
</head>
<style>
  .rule{width: 250px;float: left;padding: 8px;border: 1px solid #ccc;min-height: 250px;}
</style>
<body>
<div class="panel admin-panel">
  <div class="body-content">
    <form method="post" class="form-x" onsubmit="return false">  
      <div class="form-group">
        <div class="label">
          <label>分组名称：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" id="title" name="title" value="{{ $group->title }}" />
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>备注：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" id="bak" name="bak" value="{{ $group->bak }}" />
        </div>
      </div>

      <!-- 权限列表 -->
      <div class="form-group">
        <div class="label">
          <label>权限列表：</label>
        </div>
        <div class="field" id="auth">

          @foreach($rule_list as $va)
          <div class="rule">
          <p><input type="checkbox" @if(in_array($va->id,$group->rules)) checked @endif name="checklist" value="{{ $va->id }}">{{ $va->title }}</p>
          <ul>
              @foreach($va->child as $vo)
              <p><input type="checkbox" @if(in_array($vo->id,$group->rules)) checked @endif name="checkbox_{{$va->id}}" value="{{ $vo->id }}">{{ $vo->title }}</p>
              @endforeach
          </ul>
          </div>
          @endforeach

        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label></label>
        </div>
        <div class="field">
          <button class="button bg-main icon-check-square-o" id="edit_group"> 提交</button>
        </div>
      </div>
    </form>
  </div>
</div>
<script>

  $('input[name="checklist"]').on("click",function(){
      var id = $(this).val();
      if($(this).is(':checked')){
        $('input[name="checkbox_'+id+'"]').each(function(){
          $(this).prop("checked",true);
        });
      }else{
        $('input[name="checkbox_'+id+'"]').each(function(){
          $(this).prop("checked",false);
        });
      }
    });

  // 提交更改
  $("#edit_group").click('on',function(){
      var title = $("#title").val();
      var bak   = $("#bak").val();
      var check_list = [];
      $.each($('input:checkbox:checked'),function(){
          check_list.push($(this).val());
      });

      // 提交更改
      layer.confirm('您确定要修改权限?', {
        btn: ['确定','取消'] //按钮
      }, function(){
          $.ajax({
          type:"post",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url:"{{ url('admin/group_edit',['id'=>$group->id]) }}",
          data:{'title':title,'bak':bak,'rules':check_list},
          success:function(msg){
            if(msg.err_code == 200){
              location.reload();
            }else{
              layer.msg(msg.err_msg);
              location.reload();
            }
          }
        });
      }, function(){
         layer.msg('已取消');
      });
  })

</script>
</body>
</html>