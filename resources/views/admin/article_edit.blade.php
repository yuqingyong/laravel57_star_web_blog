<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="/static/admin/css/pintuer.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">
<script src="/static/admin/js/jquery.js"></script>
<script src="/static/admin/js/pintuer.js"></script>
<script src="/static/admin/js/layer.js"></script>
<script charset="utf-8" src="/static/admin/kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="/static/admin/kindeditor/lang/zh-CN.js"></script>
<script>
    KindEditor.ready(function(K) {
            window.editor = K.create('#editor_id');
    });
    var options = {
            cssPath : '/css/index.css',
            filterMode : true
    };
    var editor = K.create('textarea[name="content"]', options);

    html = editor.html();

    // 同步数据后可以直接取得textarea的value
    editor.sync();
    html = document.getElementById('editor_id').value; // 原生API
    html = K('#editor_id').val(); // KindEditor Node API
    html = $('#editor_id').val(); // jQuery
    // 设置HTML内容
    editor.html('HTML内容');
    // 关闭过滤模式，保留所有标签
    // KindEditor.options.filterMode = false;

    // KindEditor.ready(function(K)) {
    //         K.create('#editor_id');
    // }
</script>
</head>
<body>
<div class="panel admin-panel">
  <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>修改文章</strong></div>
  <div class="body-content">
    <form method="post" class="form-x" action="{{ url('admin/article_edit',['aid'=>$article->aid]) }}" enctype="multipart/form-data">
      @if (count($errors) > 0)
        <p style="margin: 0 auto;color:red;text-align: center;">{{ $errors->first() }}</p>
      @endif
      <div class="form-group">
        <div class="label">
          <label>标题：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="{{ $article->title }}" name="title"/>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>文章分类：</label>
        </div>
        <div class="field">
        <select class="form-control" name="cid">
			  <option value ="{{ $cat->cid }}">{{ $cat->cname }}</option>
              @foreach ($category as $cate)
              <option value ="{{ $cate->cid }}">{{ $cate->cname }}</option>
              @endforeach
			</select>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>作者：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="{{ $article->author }}" name="author"/>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>关键词：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="{{ $article->keywords }}" name="keywords"/>
        </div>
      </div>
      <div class="clear"></div>
      <div class="form-group">
        <div class="label">
          <label>排序：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" name="sort" value="{{ $article->sort }}" />
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>点击量：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" name="click" value="{{ $article->click }}" />
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>简单描述：</label>
        </div>
        <div class="field">
          <textarea type="text" class="input w50" name="description" />{{ $article->description }}</textarea>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>详情：</label>
        </div>
        <div class="field">
          <textarea id="editor_id" name="content" style="width:800px;height:500px;">{{ $article->content }}</textarea>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label></label>
        </div>
        <div class="field">
          {{ csrf_field() }}
          <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
        </div>
      </div>
    </form>
  </div>
</div>

</body>
</html>