<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>星辰博客管理后台</title>
    <meta name="keywords" content="星辰博客,喻庆勇,网站后台,后台管理,管理系统,网站模板" />
    <meta name="description" content="星辰博客" /> 
    <link rel="stylesheet" href="/static/admin/css/pintuer.css">
    <link rel="stylesheet" href="/static/admin/css/admin.css">
    <script src="/static/admin/js/jquery.js"></script>
</head>
<style>
  .has_auth{display: none;}
</style>
<body style="background-color:#f2f9fd;">
<div class="header bg-main">
  <div class="logo margin-big-left fadein-top">
    <h1><img src="/static/admin/images/y.jpg" class="radius-circle rotate-hover" height="50" alt="" />后台管理中心</h1>
  </div>
  <div class="head-l"><a class="button button-little bg-green" href="/" target="_blank"><span class="icon-home"></span> 前台首页</a>&nbsp;&nbsp;<a class="button button-little bg-red" href="{{ url('admin/login_out') }}"><span class="icon-power-off"></span> 退出登录</a>&nbsp;&nbsp;<a class="button button-little bg-blue" href="{{ url('admin/clear_cache') }}"><span class="icon-power-off"></span> 清除缓存</a></div>
</div>
<div class="leftnav">
  <div class="leftnav-title"><strong><span class="icon-list"></span>菜单列表</strong></div>
  <h2><span class="icon-user"></span>基本设置</h2>
  <ul style="display:block">
    <li><a href="{{ url('admin/edit_pass') }}" target="right"><span class="icon-caret-right"></span>修改密码</a></li>
    
  </ul> 
  <h2><span class="icon-pencil-square-o"></span>权限管理</h2>
  <ul>
    <li @if(!in_array('权限列表',$rule)) class="has_auth" @endif><a href="{{ url('admin/rule_list') }}" target="right"><span class="icon-caret-right"></span>权限列表</a></li>
    <li @if(!in_array('管理分组',$rule)) class="has_auth" @endif><a href="{{ url('admin/auth_group') }}" target="right"><span class="icon-caret-right"></span>管理分组</a></li>
    <li @if(!in_array('管理员列表',$rule)) class="has_auth" @endif><a href="{{ url('admin/admin_user') }}" target="right"><span class="icon-caret-right"></span>管理员列表</a></li>
  </ul>  
  <h2><span class="icon-pencil-square-o"></span>栏目管理</h2>
  <ul>
    <li @if(!in_array('会员列表',$rule)) class="has_auth" @endif><a href="{{ url('admin/member_list') }}" target="right"><span class="icon-caret-right"></span>会员管理</a></li>
    <li @if(!in_array('文章列表',$rule)) class="has_auth" @endif><a href="{{ url('admin/article_list') }}" target="right"><span class="icon-caret-right"></span>文章列表</a></li>
    <li @if(!in_array('闲言碎语',$rule)) class="has_auth" @endif><a href="{{ url('admin/chat_list') }}" target="right"><span class="icon-caret-right"></span>闲言碎语</a></li>
    <li @if(!in_array('用户评论',$rule)) class="has_auth" @endif><a href="{{ url('admin/comment_list') }}" target="right"><span class="icon-caret-right"></span>评论列表</a></li>      
    <li @if(!in_array('需求列表',$rule)) class="has_auth" @endif><a href="{{ url('admin/demand_list') }}" target="right"><span class="icon-caret-right"></span>需求列表</a></li>   
    <li @if(!in_array('留言列表',$rule)) class="has_auth" @endif><a href="{{ url('admin/message_list') }}" target="right"><span class="icon-caret-right"></span>留言列表</a></li>    
  </ul>
  <h2><span class="icon-pencil-square-o"></span>分类管理</h2>
  <ul>
    <li @if(!in_array('文章分类',$rule)) class="has_auth" @endif><a href="{{ url('admin/category_list') }}" target="right"><span class="icon-caret-right"></span>文章分类</a></li>
    <li @if(!in_array('文章标签',$rule)) class="has_auth" @endif><a href="{{ url('admin/tag_list') }}" target="right"><span class="icon-caret-right"></span>标签管理</a></li>
  </ul>
  <h2><span class="icon-pencil-square-o"></span>其他管理</h2>
  <ul>
    <li @if(!in_array('广告列表',$rule)) class="has_auth" @endif><a href="{{ url('admin/advert_list') }}" target="right"><span class="icon-caret-right"></span>图片列表</a></li>
    <li @if(!in_array('友情链接',$rule)) class="has_auth" @endif><a href="{{ url('admin/friendurl_list') }}" target="right"><span class="icon-caret-right"></span>友情链接</a></li>
    <li><a href="{{ url('admin/article_recovery') }}" target="right"><span class="icon-caret-right"></span>文章回收站</a></li>
  </ul>
</div>
<script type="text/javascript">
$(function(){
  $(".leftnav h2").click(function(){
	  $(this).next().slideToggle(200);	
	  $(this).toggleClass("on"); 
  })
  $(".leftnav ul li a").click(function(){
	    $("#a_leader_txt").text($(this).text());
  		$(".leftnav ul li a").removeClass("on");
		$(this).addClass("on");
  })
});
</script>
<ul class="bread">
  <li><a href="" target="right" class="icon-home"> 首页</a></li>
  <li><a href="##" id="a_leader_txt">网站信息</a></li>
  <li><b>当前语言：</b><span style="color:red;">中文</php></span>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;切换语言：<a href="##">中文</a> &nbsp;&nbsp;<a href="##">英文</a> </li>
</ul>
<div class="admin">
  <iframe scrolling="auto" rameborder="0" src="{{ url('admin/website') }}" name="right" width="100%" height="100%"></iframe>
</div>
</body>
</html>