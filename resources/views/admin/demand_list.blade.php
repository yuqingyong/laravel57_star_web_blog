<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title></title>
<link rel="stylesheet" href="/static/admin/css/pintuer.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">
<script src="/static/admin/js/jquery.js"></script>
<script src="/static/admin/js/pintuer.js"></script>
<script src="/static/admin/js/layer.js"></script>
</head>
<body>
<form id="listform">
   <div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 需求列表</strong></div>
    <table class="table table-hover text-center">
      <tr>
        <th width="100" style="text-align:left; padding-left:20px;">ID</th>
        <th>需求用户</th>
        <th>需求标题</th>
        <th>需求类型</th>
        <th>提交时间</th>
        <th>需求内容</th>
        <th>是否展示</th>
        <th width="310">操作</th>
      </tr>
		@foreach ($demands as $demand)
        <tr>
          <td style="text-align:left; padding-left:20px;"><input type="checkbox" name="id[]" value="" />{{ $demand->xid }}</td>
          <td>{{ $demand->username }}</td>
          <td>{{ $demand->title }}</td>
          <td>{{ $demand->demand_type }}</td>
          <td>{{ $demand->create_time }}</td>
          <td width="30%"><?php echo substr($demand->contents, 0,80); ?></td>
		  @switch($demand->is_show)
		  	@case(1)
		  		<td>是</td>
		  		@break
		  	@case(0)
		  		<td style="color: red;">否</td>
		  		@break
		  @endswitch
          <td>
          	<div class="button-group">
				@if ($demand->is_show == 0)
				<a class="button border-main" href="javascript:void(0)" onclick="return edit_status({{ $demand->xid }},1)"><span class="icon-edit"></span> 启用</a>
				@else
				<a class="button border-main" href="javascript:void(0)" onclick="return edit_status({{ $demand->xid }},0)"><span class="icon-edit"></span> 禁用</a>
				@endif
          		<a class="button border-red" href="javascript:void(0)" onclick="return del({{ $demand->xid }})"><span class="icon-trash-o"></span> 删除</a> 
          	</div>
          </td>
        </tr>
		@endforeach
    </table>
    <div class="page">
    	{{ $demands->links() }}
    </div>
  </div>
</form>
<script type="text/javascript">
//删除
function del(xid){
	//询问框
	layer.confirm('您确定要删除该需求吗?', {
	  btn: ['确定','取消'] //按钮
	}, function(){
	    $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type:"post",
			url:"{{ url('admin/demand_del') }}",
			data:{'xid':xid},
			success:function(msg){
				if(msg.err_code == 200){
					location.reload();
				}else{
					layer.msg(msg.err_msg);
					location.reload();
				}
			}
		});
	}, function(){
	   layer.msg('已取消');
	});

}

//是否显示
function edit_status(xid,status)
{
	layer.confirm('您确定要修改状态?', {
	  btn: ['确定','取消'] //按钮
	}, function(){
	    $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type:"post",
			url:"{{ url('admin/demand_status') }}",
			data:{'xid':xid,'status':status},
			success:function(msg){
				if(msg.err_code == 200){
					location.reload();
				}else{
					layer.msg(msg.err_msg);
					location.reload();
				}
			}
		});
	}, function(){
	   layer.msg('已取消');
	});
}

//全选
$("#checkall").click(function(){ 
  $("input[name='id[]']").each(function(){
	  if (this.checked) {
		  this.checked = false;
	  }
	  else {
		  this.checked = true;
	  }
  });
})

//批量删除
function DelSelect(){
	var Checkbox=false;
	 $("input[name='id[]']").each(function(){
	  if (this.checked==true) {		
		Checkbox=true;	
	  }
	});
	if (Checkbox){
		var t=confirm("您确认要删除选中的内容吗？");
		if (t==false) return false;		
		$("#listform").submit();		
	}
	else{
		alert("请选择您要删除的内容!");
		return false;
	}
}

//批量排序
function sorts(){
	var Checkbox=false;
	 $("input[name='id[]']").each(function(){
	  if (this.checked==true) {		
		Checkbox=true;	
	  }
	});
	if (Checkbox){	
		
		$("#listform").submit();		
	}
	else{
		alert("请选择要操作的内容!");
		return false;
	}
}

</script>
</body>
</html>