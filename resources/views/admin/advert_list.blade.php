<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title></title>
<link rel="stylesheet" href="/static/admin/css/pintuer.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">
<script src="/static/admin/js/jquery.js"></script>
<script src="/static/admin/js/pintuer.js"></script>
<script src="/static/admin/js/layer.js"></script>
</head>
<body>
  <div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 广告图片列表</strong></div>
    <div class="padding border-bottom">
      <ul class="search" style="padding-left:10px;">
        <li> <a class="button border-main icon-plus-square-o" href="{{ url('admin/advert_add') }}"> 添加广告图片</a> </li>
      </ul>
    </div>
    <table class="table table-hover text-center">
      <tr>
        <th width="100" style="text-align:left; padding-left:20px;">ID</th>
        <th>图片</th>
        <th>图片名</th>
        <th>来源</th>
        <th>url</th>
        <th>开始时间</th>
        <th>结束时间</th>
        <th>是否展示</th>
        <th width="310">操作</th>
      </tr>
		@foreach ($adverts as $advert)
        <tr>
          <td style="text-align:left; padding-left:20px;"><input type="checkbox" name="id[]" value="" />{{ $advert->mid }}</td>
          <td width="10%"><img src="{{ $advert->img }}" alt="" style="width: 275px;height: 120px;"></td>
          <td width="10%">{{ $advert->mname }}</td>
          <td width="10%">{{ $advert->from }}</td>
          <td width="10%">{{ $advert->url }}</td>
          <td width="10%">{{ date("Y-m-d H:i:s",$advert->create_time) }}</td>
          <td width="10%">{{ date("Y-m-d H:i:s",$advert->end_time) }}</td>
          @switch($advert->is_show)
          	@case(1)
          		<td>是</td>
          		@break
          	@case(0)
          		<td style="color: red;">否</td>
          		@break
          @endswitch
          <td>
          	<div class="button-group"> 
              @if ($advert->is_show == 0)
              <a class="button border-main" href="javascript:void(0)" onclick="return edit_status({{ $advert->mid }},1)"><span class="icon-edit"></span> 启用</a>
              @else
              <a class="button border-main" href="javascript:void(0)" onclick="return edit_status({{ $advert->mid }},0)"><span class="icon-edit"></span> 禁用</a>
              @endif
          		<a class="button border-main" title="编辑" href="{{ url('admin/advert_edit',['mid'=>$advert->mid]) }}"><span class="icon-edit"></span>修改</a>
          		<a class="button border-main" onclick="return del({{ $advert->mid }})"><span class="icon-edit"></span>删除</a>
          	</div>
          </td>
        </tr>
				@endforeach
    </table>
    <div class="page">
      {{ $adverts->links() }}
    </div>
  </div>
</body>
<script>
  //删除
function del(mid){
  //询问框
  layer.confirm('您确定要删除该广告吗?', {
    btn: ['确定','取消'] //按钮
  }, function(){
      $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},	
      type:"post",
      url:"{{ url('admin/advert_del') }}",
      data:{'mid':mid},
      success:function(msg){
        if(msg.err_code == 200){
        	location.reload();
        }else{
        	layer.msg(msg.err_msg);
        	location.reload();
        }
      }
    });
  }, function(){
     layer.msg('已取消');
  });

}



//是否显示
function edit_status(mid,is_show)
{

  layer.confirm('您确定要修改状态?', {
    btn: ['确定','取消'] //按钮
  }, function(){
      $.ajax({
      type:"post",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
      url:"{{ url('admin/advert_status') }}",
      data:{'mid':mid,'is_show':is_show},
      success:function(msg){
        if(msg.err_code == 200){
        	location.reload();
        }else{
        	layer.msg(msg.err_msg);
        	location.reload();
        }
      }
    });
  }, function(){
     layer.msg('已取消');
  });
  
  
}
</script>
</html>