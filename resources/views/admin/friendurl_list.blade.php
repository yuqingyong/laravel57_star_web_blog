<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="/static/admin/css/pintuer.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">
<script src="/static/admin/js/jquery.js"></script>
<script src="/static/admin/js/pintuer.js"></script>
<script src="/static/admin/js/layer.js"></script>
</head>
<body>
  <div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 友情链接列表</strong></div>
    <div class="padding border-bottom">
      <ul class="search" style="padding-left:10px;">
        <li> <a class="button border-main icon-plus-square-o" href="{{ url('admin/friendurl_add') }}"> 添加友情链接</a> </li>
      </ul>
    </div>
    <table class="table table-hover text-center">
      <tr>
        <th width="100" style="text-align:left; padding-left:20px;">ID</th>
        <th>友情链接名</th>
        <th>url</th>
        <th>排序</th>
        <th width="310">操作</th>
      </tr>
				@foreach ($friendurls as $friendurl)
        <tr>
          <td style="text-align:left; padding-left:20px;"><input type="checkbox" name="id[]" value="" />{{ $friendurl->id }}</td>
          <td width="10%">{{ $friendurl->lname }}</td>
          <td width="10%">{{ $friendurl->url }}</td>
          <td width="10%">{{ $friendurl->sort }}</td>
          <td>
          	<div class="button-group"> 
          		<a class="button border-main" title="编辑" href="{{ url('admin/friendurl_edit',['id'=>$friendurl->id]) }}"><span class="icon-edit"></span>修改</a>
          		<a class="button border-main" onclick="return del({{ $friendurl->id }})"><span class="icon-edit"></span>删除</a>
          	</div>
          </td>
        </tr>
				@endforeach
    </table>
    <div class="page">
      {{ $friendurls->links() }}
    </div>
  </div>
</body>
<script>
  //删除
function del(id){
  //询问框
  layer.confirm('您确定要删除该链接吗?', {
    btn: ['确定','取消'] //按钮
  }, function(){
      $.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},	
				type:"post",
				url:"{{ url('admin/friendurl_del') }}",
				data:{'id':id},
				success:function(msg){
					if(msg.err_code == 200){
						location.reload();
					}else{
						layer.msg(msg.err_msg);
						location.reload();
					}
				}
    });
  }, function(){
     layer.msg('已取消');
  });

}

</script>
</html>