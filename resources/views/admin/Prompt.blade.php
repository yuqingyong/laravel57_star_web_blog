<!DOCTYPE html>
<html lang="zh-cn" >
<head>
    <meta charset="UTF-8">
    <title>系统提示</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css"> 
    <!-- Google Font -->
    <style>
        .color-palette {
            height: 35px;
            line-height: 35px;
            text-align: center;
        }
        .color-palette-set {
            margin-bottom: 15px;
        }
        .color-palette span {
            display: none;
            font-size: 12px;
        }
        .color-palette:hover span {
            display: block;
        }
        .color-palette-box h4 {
            position: absolute;
            top: 100%;
            left: 25px;
            margin-top: -40px;
            color: rgba(255, 255, 255, 0.8);
            font-size: 12px;
            display: block;
            z-index: 7;
        }
		.box-title{font-size: 18px;}
		.box-body{background-color: #00a65a;padding: 10px;color: #fff;}
		.jump_now{color: #fff;text-decoration: underline;}
    </style>
</head>
<body>
<div style="margin:auto; width: 50%; height: auto; overflow: hidden;">
    <div class="box box-default" style="margin-top: 20%;">
        <div class="box-header with-border">
            <i class="glyphicon glyphicon-volume-up"></i>
            <span class="box-title">提示</span>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if ($data['status']=='error')
            <div class="callout callout-danger">
                <h4>错误</h4>
                <p>{{$data['message']}}</p>
                <p>浏览器页面将在<b id="loginTime_error">{{ $data['jumpTime'] }}</b>秒后跳转......<a href="javascript:void(0);" class="jump_now">立即跳转</a> </p>
            </div>
            @endif
            @if ($data['status']=='continue')
            <div class="callout callout-info">
                <h4>未完成，继续</h4>
                <p>{{$data['message']}}</p>
                <p>浏览器页面将在<b id="loginTime_continue">{{ $data['jumpTime'] }}</b>秒后跳转......<a href="javascript:void(0);" class="jump_now">立即跳转</a> </p>
            </div>
            @endif
            @if ($data['status']=='warning')
            <div class="callout callout-warning">
                <h4>警告</h4>
                <p>{{$data['message']}}</p>
                <p>浏览器页面将在<b id="loginTime_warning">{{ $data['jumpTime'] }}</b>秒后跳转......<a href="javascript:void(0);" class="jump_now">立即跳转</a> </p>
            </div>
            @endif
            @if ($data['status']=='success')
            <div class="callout callout-success">
                <h4>成功</h4>
                <p>{{$data['message']}}</p>
                <p>浏览器页面将在<b id="loginTime_success">{{ $data['jumpTime'] }}</b>秒后跳转......<a href="javascript:void(0);" class="jump_now">立即跳转</a> </p>
            </div>
            @endif
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
</body>
</html>
<!-- jQuery 3 -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<!--本页JS-->
<script type="text/javascript">
    $(function(){
        //循环倒计时，并跳转
        var url = "{{ $data['url'] }}";
        var loginTimeID='loginTime_'+'{{$data['status']}}';
        //alert(loginTimeID);return;
        var loginTime = parseInt($('#'+loginTimeID).text());
        console.log(loginTime);
        var time = setInterval(function(){
            loginTime = loginTime-1;
            $('#'+loginTimeID).text(loginTime);
            if(loginTime==0){
                clearInterval(time);
                window.location.href=url;
            }
        },1000);
    });
    //点击跳转
    $('.jump_now').click(function () {
        var url = "{{ $data['url'] }}";
        window.location.href=url;
    });
</script>