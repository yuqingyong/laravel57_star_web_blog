<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title></title>
<link rel="stylesheet" href="/static/admin/css/pintuer.css">
<link rel="stylesheet" href="/static/admin/css/admin.css">
<script src="/static/admin/js/jquery.js"></script>
<script src="/static/admin/js/pintuer.js"></script>
<script src="/static/admin/js/layer.js"></script>
</head>
<body>
<form id="listform">
   <div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 分类列表</strong></div>
    <div class="padding border-bottom">
      <ul class="search" style="padding-left:10px;">
        <li> <a class="button border-main icon-plus-square-o" href="{{ url('admin/category_add') }}"> 添加分类</a> </li>
      </ul>
    </div>
    
    <table class="table table-hover text-center">
      <tr>
        <th width="100" style="text-align:left; padding-left:20px;">ID</th>
        <th>分类名</th>
        <th width="310">操作</th>
      </tr>
		@foreach ($categorys as $category)
        <tr>
          <td style="text-align:left; padding-left:20px;"><input type="checkbox" name="id[]" value="" />{{ $category->cid }}</td>
          <td width="30%">{{ $category->cname }}</td>
          <td>
          	<div class="button-group">
          		<a class="button border-main" href="{{ url('admin/category_edit',['cid'=>$category->cid]) }}"><span class="icon-edit"></span> 修改</a>
          		<a class="button border-red" href="javascript:void(0)" onclick="return del({{ $category->cid }})"><span class="icon-trash-o"></span> 删除</a> 
          	</div>
          </td>
        </tr>
		@endforeach
    </table>
    <div class="page">
    	{{ $categorys->links() }}
    </div>
  </div>
</form>
<script type="text/javascript">
//删除
function del(cid){
	//询问框
	layer.confirm('您确定要删除该分类吗?', {
	  btn: ['确定','取消'] //按钮
	}, function(){
	    $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type:"post",
			url:"{{ url('admin/category_del') }}",
			data:{'cid':cid},
			success:function(msg){
				if(msg.err_code == 200){
					location.reload();
				}else{
					layer.msg(msg.err_msg);
					location.reload();
				}
			}
		});
	}, function(){
	   layer.msg('已取消');
	});

}


//全选
$("#checkall").click(function(){ 
  $("input[name='id[]']").each(function(){
	  if (this.checked) {
		  this.checked = false;
	  }
	  else {
		  this.checked = true;
	  }
  });
})

//批量删除
function DelSelect(){
	var Checkbox=false;
	 $("input[name='id[]']").each(function(){
	  if (this.checked==true) {		
		Checkbox=true;	
	  }
	});
	if (Checkbox){
		var t=confirm("您确认要删除选中的内容吗？");
		if (t==false) return false;		
		$("#listform").submit();		
	}
	else{
		alert("请选择您要删除的内容!");
		return false;
	}
}

//批量排序
function sorts(){
	var Checkbox=false;
	 $("input[name='id[]']").each(function(){
	  if (this.checked==true) {		
		Checkbox=true;	
	  }
	});
	if (Checkbox){	
		
		$("#listform").submit();		
	}
	else{
		alert("请选择要操作的内容!");
		return false;
	}
}

</script>
</body>
</html>